#include "player.h"

Player::Player(){}

void Player::save(){
    std::string location = "saves/";
    location.append(name);
    std::string playerFile = location;
    playerFile.append("/player.dat");
    saveCharacter(playerFile.c_str());

    std::string achievementFile = location;
    achievementFile.append("/achievements.dat");
    achievements.saveAchievements(achievementFile.c_str());

    //loads the characters encounter stats
    std::string creepFile = location;
    creepFile.append("/creeps.dat");
    saveCreeps(creepFile.c_str());

    std::string itemFile = location;
    itemFile.append("/items.dat");
    saveItems(itemFile.c_str(),items);

    std::string marketFile = location;
    marketFile.append("/market.dat");
    saveItems(marketFile.c_str(),marketItems);
}

Player::Player(std::string name)
{
    this->name = name;
    std::string location = "saves/";
    location.append(name);
    std::string playerFile = location;
    playerFile.append("/player.dat");

    if(!loadCharacter(playerFile.c_str())){
        return;
    }
    //loads all the current achievements
    std::string achievementFile = location;
    achievementFile.append("/achievements.dat");
    achievements = AchievementList(achievementFile.c_str());
    //loads the characters encounter stats
    std::string creepFile = location;
    creepFile.append("/creeps.dat");
    loadCreeps(creepFile.c_str());

    std::string itemFile = location;
    itemFile.append("/items.dat");
    loadItems(itemFile.c_str(),items);

    std::string marketFile = location;
    marketFile.append("/market.dat");
    loadItems(marketFile.c_str(),marketItems);
}

void Player::loadItems(const char * filename,std::vector<Item*> &items){
    std::ifstream itemFile(filename);
    if(!itemFile.is_open()){
        std::cout << "could not read " << filename<< std::endl;
        return;
    }
    std::string line;
    std::string item;
    std::string attack;
    int baseStat;
    int str;
    int sta;
    int agi;
    bool wearing;
    int type;
    int value;
    bool augmented;

    std::getline(itemFile,line);
    item = line;
    std::getline(itemFile,line);
    attack = line;
    std::getline(itemFile,line);
    std::stringstream ss(line);
    ss >> baseStat >> str >> sta >> agi >> wearing >> type >> value >> augmented;
    while(!ss.fail()){
        items.push_back(new Item(item,attack,baseStat,str,sta,agi,wearing,type,value,augmented));
        std::getline(itemFile,line);
        item = line;
        std::getline(itemFile,line);
        attack = line;
        std::getline(itemFile,line);
        std::stringstream ss(line);
        ss >> baseStat >> str >> sta >> agi >> wearing >> type >> value >> augmented;
        if(ss.fail())
            break;
    }
    itemFile.close();
}

void Player::saveItems(const char * filename, std::vector<Item*> &items){
    std::ofstream itemFile;
    itemFile.open (filename);
    for(unsigned int i=0;i<items.size();i++){
        Item * t = items.at(i);
        std::string lineOne = t->getName();
        std::string lineTwo = t->getAttack();
        std::string line = "";
        std::stringstream ss(line);
        ss << t->getBaseStat() << " " << t->getStrength() << " " << t->getStamina() << " " << t->getAgility()
           << " " << t->getWearing() << " " << t->getType() << " " << t->getValue() << " " << t->getAugmented();
        itemFile << lineOne << std::endl << lineTwo << std::endl << ss.str() << std::endl;
    }
    itemFile.close();
}

bool Player::loadCharacter(const char* filename){

    std::ifstream file(filename);
    if(!file.is_open()){
        std::cout << "You Have Started a new game on normal mode (hard is unlocked upon normal mode completion)" << std::endl;
        createNewPlayer();
        InputReader::pressEnter();
        return false;
    }
    std::string line;
    std::getline(file,line);
    std::stringstream ss(line);

    ss >> level >> honour >> health >> agility >> damage >> gearArmour >> coreStrength >> coreStamina >> coreAgility >> experience >> experienceRequired >> gold >> potions
          >> time >> kills >> deaths >> flees >> achievementPoints >> questType >> questTarget >> questProgress >> questGoal >> questPoints >> companion;
    if(ss.fail()){
        std::cout << "corrupted player file, cannot continue";
        exit(1);
    }
    std::getline(file,line);
    companionName = line;
    std::getline(file,line);
    std::stringstream sstwo(line);
    sstwo >> passiveCompanion >> difficulty;
    file.close();
    return true;

}

void Player::saveCharacter(const char * filename){
    totalStats();
    std::ofstream file;
    file.open (filename);
    std::string line = "";
    std::stringstream ss(line);
    ss << level << " " << honour << " " << health << " " << agility << " " << damage << " " << gearArmour << " "
       << coreStrength << " " << coreStamina << " " << coreAgility << " " << experience << " " << experienceRequired
       << " " << gold << " " << potions << " " << time << " " << kills << " " << deaths << " " << flees << " "
       << achievementPoints << " " << questType << " " << questTarget << " " << questProgress << " " << questGoal
       << " " << questPoints << " " << companion;
    file << ss.str() << std::endl << companionName << std::endl;
    ss.str("");
    ss << passiveCompanion << " " << difficulty;
    file << ss.str();
    file.close();
}

/**
*checks whether an achievement was earned if it was then displays a
*Message to the user describing the achievement to them
*/
void Player::earnAchievements()
{

    if(kills>=1)
         achievementPoints += achievements.earn("Not A Noob");
    if(kills>=20)
         achievementPoints += achievements.earn("Knows How To Press 1");
    if(kills>=100)
         achievementPoints += achievements.earn("Button Masher");
    if(kills>=500)
         achievementPoints += achievements.earn("Carnage");
    if(kills>=1000)
         achievementPoints += achievements.earn("Kill Collector");
    if(kills>=2000)
         achievementPoints += achievements.earn("God Of War");
    if(flees>=10)
         achievementPoints += achievements.earn("Light Footed");
    if(flees>=100)
         achievementPoints += achievements.earn("Pansy");
    if(gold>=20)
         achievementPoints += achievements.earn("Double Platinum");
    if(gold>=1000)
         achievementPoints += achievements.earn("Just Grand");
    if(gold>=10000)
         achievementPoints += achievements.earn("Ka-Ching!");
    if(gold>=100000)
         achievementPoints += achievements.earn("What Are You Saving For?");
    if(gold>=1000000)
         achievementPoints += achievements.earn("Millionaire");
    if(gold>=10000000)
         achievementPoints += achievements.earn("Purposeless Wealth");
    if(gold>=100000000)
         achievementPoints += achievements.earn("Sir Spamalot");
    if(strength>=20)
         achievementPoints += achievements.earn("Strong");
    if(strength>=50)
         achievementPoints += achievements.earn("Very Strong");
    if(strength>=100)
         achievementPoints += achievements.earn("Super Strength");
    if(strength>=200)
         achievementPoints += achievements.earn("Mr Apollo");
    if(stamina>=20)
         achievementPoints += achievements.earn("Healthy");
    if(stamina>=50)
         achievementPoints += achievements.earn("Hard To Kill");
    if(stamina>=100)
         achievementPoints += achievements.earn("Die Hard");
    if(stamina>=200)
         achievementPoints += achievements.earn("Tarrasque");
    if(agility>=20)
         achievementPoints += achievements.earn("Quick");
    if(agility>=50)
         achievementPoints += achievements.earn("Speedy");
    if(agility>=100)
         achievementPoints += achievements.earn("Ninja");
    if(agility>=200)
         achievementPoints += achievements.earn("Time Traveller");
    if(strength>=20&&stamina>=20&&agility>=20)
         achievementPoints += achievements.earn("Competent");
    if(strength>=50&&stamina>=50&&agility>=50)
         achievementPoints += achievements.earn("Powerful");
    if(strength>=100&&stamina>=100&&agility>=100)
         achievementPoints += achievements.earn("Thats A Big Mountain Lion");
    if(strength>=200&&stamina>=200&&agility>=200)
         achievementPoints += achievements.earn("Godlike");
    if(gearArmour>=1)
         achievementPoints += achievements.earn("Armoured");
    if(gearArmour>=360)
         achievementPoints += achievements.earn("Not A Chink");
    if(potions>=10)
         achievementPoints += achievements.earn("Well Stocked");
    if(potions>=100)
         achievementPoints += achievements.earn("The Regenerator");
    if(level>=5)
         achievementPoints += achievements.earn("Baby Steps");
    if(level>=20)
         achievementPoints += achievements.earn("Getting Into It");
    if(level>=50)
         achievementPoints += achievements.earn("Loyal Player");
    if(level>=75)
         achievementPoints += achievements.earn("Dedicated");
    if(level>=100)
         achievementPoints += achievements.earn("Overpowered");
    if(honour>=10)
         achievementPoints += achievements.earn("Duelist");
    if(honour>=50)
         achievementPoints += achievements.earn("Well Built");
    if(honour>=100)
         achievementPoints += achievements.earn("Pimped");
    bool completed = true;
    for(unsigned int i=0;i<creeps.size();i++)
        if(!creeps.at(i)->isEncountered())
            completed = false;
    if(completed)
         achievementPoints += achievements.earn("Gotta Find Them All!");
    if( achievementPoints>=10)
         achievementPoints += achievements.earn("Purpose");
    if( achievementPoints>=100)
         achievementPoints += achievements.earn("Triple Figures");
    if( achievementPoints>=200)
         achievementPoints += achievements.earn("Achiever");
    if( achievementPoints>=300)
         achievementPoints += achievements.earn("Good Achiever");
    if( achievementPoints>=500)
         achievementPoints += achievements.earn("Bronze Achiever");
    if( achievementPoints>=750)
         achievementPoints += achievements.earn("Silver Achiever");
    if( achievementPoints>=1000)
         achievementPoints += achievements.earn("Gold Achiever");
    if( achievementPoints>=1500)
         achievementPoints += achievements.earn("Platinum Achiever");
    if( achievementPoints>=2400)
         achievementPoints += achievements.earn("DM");
    for(unsigned int i=0;i<achievements.achievements.size();i++){
        Achievement a = achievements.achievements.at(i);
        if(!a.earned&&a.name.compare("Completionist"))
            return;
    }
    achievementPoints += achievements.earn("Completionist");
}

void Player::loadCreeps(const char * filename){
    addCreeps();
    std::ifstream creepFile(filename);
    if(!creepFile.is_open()){
        std::cout << "could not read " << filename<< std::endl;
        return;
    }
    std::string line;

    bool encountered;
    int kills;
    int deaths;
    int flees;

    std::getline(creepFile,line);
    std::stringstream ss(line);
    ss >> encountered >> kills >> deaths >> flees;
    creeps.at(0)->setEncounterStats(encountered,kills,deaths,flees);

    for(unsigned int i=1;!ss.fail()&&i<creeps.size();i++)
    {
        std::getline(creepFile,line);
        std::stringstream ss(line);
        ss >> encountered >> kills >> deaths >> flees;
        creeps.at(i)->setEncounterStats(encountered,kills,deaths,flees);
    }

}
void Player::saveCreeps(const char* filename)
{
    std::ofstream creepFile;
    creepFile.open (filename);
    for(unsigned int i=0;i<creeps.size();i++){
        std::string line = "";
        std::stringstream ss(line);
        Creep * c = creeps.at(i);
        ss << c->isEncountered() << " " << c->getKills() << " " << c->getDeaths() << " " << c->getFlees();
        creepFile << ss.str() << std::endl;
    }
    creepFile.close();
}

Creep* Player::getCreep(int index){
    int d = difficulty;

    if(index==0){
        creeps.at(0)->initilize(50*d,8*d,10,"flails at you","",0,20*d,1*d,0.05,1);
        return  creeps.at(0);}
    if(index==1){
        creeps.at(1)->initilize(30*d,16*d,14,"lunges at you","",0,30*d,5*d,0.02,19);
        return  creeps.at(1);}
    if(index==2){
        creeps.at(2)->initilize(60*d,10*d,12,"jabs at you","",0,40*d,5*d,0.1,4);
        return  creeps.at(2);}
    if(index==3){
        creeps.at(3)->initilize(40*d,12*d,14,"paws excitedly at you","",0,30*d,0*d,0.05,6);
        return  creeps.at(3);}
    if(index==4){
        creeps.at(4)->initilize(100*d,12*d,12,"headbuts you","",0,50*d,0*d,0.1,10);
        return  creeps.at(4);}
    if(index==5){
        creeps.at(5)->initilize(50*d,10*d,agility,"spins around hitting you","metal cap",20,40*d,10*d,0,0);
        return  creeps.at(5);}
    if(index==6){
        creeps.at(6)->initilize(50*d,15*d,30,"slaps you","",0,50*d,20*d,0.1,12);
        return  creeps.at(6);}
    if(index==7){
        creeps.at(7)->initilize(60*d,12*d,16,"hits you","",0,60*d,30*d,0,0);
        return  creeps.at(7);}
    if(index==8){
        creeps.at(8)->initilize(100*d,25*d,10,"whacks you","",0,70*d,50*d,0.1,14);
        return  creeps.at(8);}
    if(index==9){
        creeps.at(9)->initilize(140*d,25*d,20,"spears you","chainmail",20,100*d,80*d,0.2,18);
        return  creeps.at(9);}
    if(index==10){
        creeps.at(10)->initilize(250*d,30*d,10,"slams you","thick bark",10,200*d,100*d,0.1,20);
        return  creeps.at(10);}
    if(index==11){
        creeps.at(11)->initilize(80*d,40*d,40,"punches you in the face","",0,300*d,100*d,0,0);
        return  creeps.at(11);}
    if(index==12){
        creeps.at(12)->initilize(200*d,30*d,30,"throws a banana at you","",0,400*d,0*d,0.1,26);
        return  creeps.at(12);}
    if(index==13){
        creeps.at(13)->initilize(500*d,40*d,20,"splats you","amorphous shell",50,800*d,200*d,0.1,30);
        return  creeps.at(13);}
    if(index==14){
        creeps.at(14)->initilize(400*d,80*d,40,"paws at you","",0,1000*d,100*d,0.1,32);
        return  creeps.at(14);}
    if(index==15){
        creeps.at(15)->initilize((health + (int)((0.5)*(damage)) - gearArmour),damage,agility,"attacks","armour",gearArmour,level*100*d,level*100*d,0.5,(int)(0.4*level)+19);
        return  creeps.at(15);}
    if(index==16){
        creeps.at(16)->initilize(200*d,100*d,100,"sticks you in the vitals","steely presence",40,1400*d,300*d,0.2,32);
        return  creeps.at(16);}
    if(index==17){
        creeps.at(17)->initilize(1000*d,250*d,10,"crushes you","stony exterior",100,1800*d,400*d,0.1,36);
        return  creeps.at(17);}
    if(index==18){
        creeps.at(18)->initilize(level*50*d,level*160*d,1+(int)((Helper::rd()+0.2)*level),"EXPLODES IN YOUR FACE","",0,100*level*d,50*level*d,0.2,(int)(0.4*level)+19);
        return  creeps.at(18);}
    if(index==19){
        creeps.at(19)->initilize(1000*d,200*d,60,"cuts you up","gleaming full plate",200,10000*d,10000*d,1,39);
        return  creeps.at(19);}
    if(index==20){
        creeps.at(20)->initilize(2*d,314159*d,10,"sits there","",0,10*d,0*d,0.001,1);
        return  creeps.at(20);}
    if(index==21){
        creeps.at(21)->initilize(2000*d,(300+gearArmour)*d,80,"drains your soul","ceremonial fullplate",200,10000*d,2000*d,1,49);
        return  creeps.at(21);}
    if(index==22){
        creeps.at(22)->initilize(10000*d,600*d,50,"whacks you with his club","thick skin",100,10000*d,10000*d,0.8,49);
        return  creeps.at(22);}
    if(index==23){
        creeps.at(23)->initilize(6000*d,600*d,100,"burninates the country side","dragon scales",1000,10000*d,20000*d,1,49);
        return  creeps.at(23);}
    if(index==24){
        creeps.at(24)->initilize(1*d,800*d,1000,"penetrates your brain","",0,10000*d,2000*d,0.5,49);
        return  creeps.at(24);}
    if(index==25){
        creeps.at(25)->initilize(6000*d,(500+gearArmour)*d,100,"drains your life force healing him and hurting you","silvery fullplate",600,20000*d,25000*d,1,59);
        return  creeps.at(25);}
    if(index==26){
        creeps.at(26)->initilize(12000*d,1000*d,200,"warhammers you in the face","thick hide",1000,25000*d,30000*d,1,59);
        return  creeps.at(26);}
    if(index==27){
        creeps.at(27)->initilize(20000*d,1500*d,50,"crushes you","diamond body",2000,20000*d,40000*d,1,59);
        return  creeps.at(27);}
    if(index==28){
        creeps.at(28)->initilize(15000*d,1200*d,300,"fries you","scales",1500,30000*d,30000*d,1,59);
        return  creeps.at(28);}
    if(index==29){
        int godKills =  creeps.at(29)->getKills();
        creeps.at(29)->initilize((50000+60000*godKills)*d,(2000+300*godKills)*d,300+90*godKills,"Smites you","immortal shimmer",4000+3000*godKills,(100000+60000*godKills)*d,(100000+60000*godKills)*d,1,59);
        return  creeps.at(29);}
    if(index==30){
        creeps.at(30)->initilize(1*d,1*d,10,"goes for the eyes","",0,1*d,1*d,0,1);
        return  creeps.at(30);}
    if(index==31){
        creeps.at(31)->initilize(500000,30000,400,"touches you with his noodley appendage","",0,200000,250000,1,59);
        return  creeps.at(31);}
    if(index==32){
        creeps.at(32)->initilize(500000,12000,400,"drains your soul healing him and hurting you","soul plate",40000,100000,300000,1,59);
        return  creeps.at(32);}
    if(index==33){
        creeps.at(33)->initilize(400000,40000,100,"heals himself","Buddha's serendipity",100000,100000,330000,1,59);
        return  creeps.at(33);}
    if(index==34){
        creeps.at(34)->initilize(200000,8000,1000,"jabs at you","",0,80000,200000,1,59);
        return  creeps.at(34);}
    if(index==35){
        creeps.at(35)->initilize(800000,10000,600,"zaps you","Zeus's tunic",50000,100000,350000,1,59);
        return  creeps.at(35);}
    if(index==36){
        creeps.at(36)->initilize(1000000,20000,400,"roundhouse kicks you","awesomeness",50000,250000,400000,1,59);
        return  creeps.at(36);}
    if(index==37){creeps.at(37)->initilize(4000*d,(0+gearArmour)*d,50,"summons minions on you","frail skeleton",100,5000*d,1000*d,1,49);
        return  creeps.at(37);}
    if(index==38){creeps.at(38)->initilize(500*d,50*d+gearArmour,100,"claws at you for","shadowy mist",0,1,1,0,19);
        return  creeps.at(38);}
    std::cout << "error that does not correspond to an existing creep" << std::endl;
    return NULL;
}

Creep* Player::getMegaCreep(int index)
{
    Creep *c = getCreep(index);
    if(!(c->equals("the doppelganger")||c->equals("the ticking easter egg")))
        c->initilize(2*c->getLife(), 2*c->getDamage(), c->getAgility(), c->getAttack(), c->getArmour(), c->getDr(), c->getXp(), c->getGp(), c->getLootChance(), c->getLootValue());
    return c;
}

Creep* Player::getCreep(std::string name){
    //runs through the list of creeps until a creep with this name is found
    for(unsigned int i = 0;i<creeps.size();i++)
        if(!creeps.at(i)->getName().compare(name))
            //when it is found then the get creep method is run which returns the creep
            return getCreep(i);
    //error message is printed if the program reaches this point and null is returned
    std::cout << "oops i spelled the name " << name << " wrong in the code using the get method" << std::endl;
    return NULL;
}

void Player::addCreeps(){
    creeps. push_back(new Creep("the weak test subject"));//0
    creeps. push_back(new Creep("the furious bunny"));//1
    creeps. push_back(new Creep("the average joe"));//2
    creeps. push_back(new Creep("the overly friendly dog"));//3
    creeps. push_back(new Creep("the big pig"));//4
    creeps. push_back(new Creep("the training dummy"));//5
    creeps. push_back(new Creep("the agile test subject"));//6
    creeps. push_back(new Creep("the naked goblin"));//7
    creeps. push_back(new Creep("the strong test subject"));//8
    creeps. push_back(new Creep("the goblin soldier"));//9
    creeps. push_back(new Creep("the treeman"));//10
    creeps. push_back(new Creep("the floating fist"));//11
    creeps. push_back(new Creep("the random angry monkey"));//12
    creeps. push_back(new Creep("the blob"));//13
    creeps. push_back(new Creep("the grizzly bear"));//14
    creeps. push_back(new Creep("the doppelganger"));//15
    creeps. push_back(new Creep("the quiet ninja"));//16
    creeps. push_back(new Creep("the rock golem"));//17
    creeps. push_back(new Creep("the ticking easter egg"));//18
    creeps. push_back(new Creep("the arena champion"));//19
    creeps. push_back(new Creep("the lemon"));//20
    creeps. push_back(new Creep("the undead arena champion"));//21
    creeps. push_back(new Creep("the cyclops"));//22
    creeps. push_back(new Creep("the green dragon"));//23
    creeps. push_back(new Creep("the gamma ray"));//24
    creeps. push_back(new Creep("the vampire lord"));//25
    creeps. push_back(new Creep("the minotaur"));//26
    creeps. push_back(new Creep("the diamond golem"));//27
    creeps. push_back(new Creep("the black dragon"));//28
    creeps. push_back(new Creep("the GOD"));//29
    creeps. push_back(new Creep("the minute giant star gerbil"));//30
    creeps. push_back(new Creep("The giant flying spaghetti monster"));//31
    creeps. push_back(new Creep("Hades"));//32
    creeps. push_back(new Creep("Buddha"));//33
    creeps. push_back(new Creep("Bruce Lee"));//34
    creeps. push_back(new Creep("Zeus"));//35
    creeps. push_back(new Creep("Chuck Norris"));//36
    creeps. push_back(new Creep("the lich"));//37
    creeps. push_back(new Creep("a minion"));//38

}

void Player::printCreepStats(){
    std::cout << std::endl << std::endl;
    for(unsigned int i=0;i<creeps.size();i++)
        getCreep(i)->printStats();
}

void Player::createNewPlayer()
    {
        //the level is initially 0 before the player goes through the level up method
        level = 0;

        //sets the experience to 0
        experience = 0;

        //sets the base health of the player as 40
        health = 80;

        //sets the required experience to level
        experienceRequired = 0;

        //the stats from the starting gear gear


        //sets the base stats all to 10
        coreStrength = 10;
        coreStamina = 10;
        coreAgility = 10;

        //sets the kills deaths and flees to 0 as the character hasn't done anything yet
        kills = 0;
        deaths = 0;
        flees = 0;

        //the character starts with 0 honour
        honour = 0;

        //the character starts with 3 potions
        potions = 3;

        //the character starts with 0 time it has not been through any turns yet
        time = 1;

        //give the player 10 gold to start with
        gold = 10;

        //the player starts with 0 achievement points
        achievementPoints = 0;

        //adds the starting items to the Items ArrayList
        items.push_back(new Item("a stick", "stick",1,0,0,0,true,1,0,false));
        items.push_back(new Item("rags", "",0,0,0,0,true,2,0,false));
        items.push_back(new Item("thin hood", "",0,0,0,0,true,3,0,false));
        items.push_back(new Item("more rags", "",0,0,0,0,true,4,0,false));
        items.push_back(new Item("hobo gloves", "",0,0,0,0,true,5,0,false));
        items.push_back(new Item("loincloth", "",0,0,0,0,true,6,0,false));

        achievements = AchievementList();
        addCreeps();
        //get the stats on the starting gear
        gearStats();

        //get the total stats of the character
        totalStats();

        questType = 0;
        questTarget = 0;
        questProgress = 0;
        questGoal = 0;
        questPoints = 0;

        companion = 30;
        companionName = "Obo";
        passiveCompanion = false;

        difficulty = 1;
        save();
    }

void Player::gearStats()
{
    gearStrength = 0;
    gearStamina = 0;
    gearAgility = 0;
    gearArmour = 0;
    gearAttack = 0;

    for(unsigned int i=0;i<items.size();i++)
    {
        Item * a = items.at(i);
        if(a->getWearing())
        {
            gearStrength += a->getStrength();
            gearStamina += a->getStamina();
            gearAgility += a->getAgility();
            if(a->getType()!=1||achievements.isEarned("Not A Chink"))
                gearArmour += a->getBaseStat();
            if(a->getType()==1)
            {
                weapon = a;
                gearAttack += a->getBaseStat();
                attack = a->getAttack();
            }
            else if(a->getType()==2)
                body = items.at(i);
            else if(a->getType()==3)
                head = items.at(i);
            else if(a->getType()==4)
                shoulders = a;
            else if(a->getType()==5)
                hands = a;
            else if(a->getType()==6)
                legs = a;
            if(achievements.isEarned("Slayer II"))
                gearAttack += (int)(gearArmour*0.1);

        }
    }
}

void Player::totalStats()
{
    gearStats();
    strength = gearStrength + coreStrength;
    stamina = gearStamina + coreStamina;
    agility = gearAgility + coreAgility;
    damage = (int)(gearAttack*strength/(10+(double)level/10.0)) + 10;
    health = 2*stamina*level+80;
    damage *= getDamageMultiplier();
    if(achievements.isEarned("Slayer I"))
        damage = (int)(damage*1.1);
    if(achievements.isEarned("Tank I"))
        health = (int)(health*1.1);
}

double Player::getDamageMultiplier()
{
    damageMultiplier=1;
    if(achievements.isEarned("Carnage"))
        damageMultiplier = 1+kills*0.001;
    if(damageMultiplier>4)
        damageMultiplier = 4;
    return damageMultiplier;
}

int Player::xpRequired(){
    return experienceRequired - experience;
}

int Player::scalingCost(){
    return (int)(10*level*(1+level/10)*(1+level/10));
}

double Player::criticalChance()
{
    double critChance = 0.05*((agility-9)/(double)level);
    if(achievements.isEarned("Ninja II"))
        critChance = 0.1*((agility-9)/(double)level);
    if(achievements.isEarned("Ninja I"))
        critChance +=0.1;

    return critChance;
}

int Player::numberOfItem(std::string name,int type){
    int numberOfItem = 0;
    for(unsigned int i=0;i<items.size();i++)
        if(!items.at(i)->getName().compare(name)&&items.at(i)->getType()==type)
            numberOfItem++;
    return numberOfItem;
}

void Player::removeItems(std::string name,int type,int number){
    int removed = 0;
    for(unsigned int i=0;i<items.size()&&removed<number;i++)
        if(!items.at(i)->getName().compare(name)&&items.at(i)->getType()==type){
            delete(items.at(i));
            items.erase(items.begin()+i);
            removed++;
        }
}

void Player::killed(Creep *c){
    kills++;
    c->result(1);
}

void Player::diedTo(Creep *c){
    deaths++;
    c->result(2);
}

void Player::fledFrom(Creep *c){
    flees++;
    c->result(3);
}

int Player::getCreepPosition(Creep *c){
    for(unsigned int i=0;i<creeps.size();i++){
        if(creeps.at(i)->equals(c))
            return i;
    }
    return -1;
}

void Player::levelUp()
{
    if(level>=100)
        return;
    while(experience>= experienceRequired)
    {//this level up loop runs when experience reaches a experience required
        //this is where the characters level increases by 1, health increases, experienceRequired increases
        //and the character is given 3 or more points to spend
        level++;
        if(level>=100)
            std::cout << "you have reached the maximum level" << std::endl;
        if(level%10==0)
            talents();
        experienceRequired += 40*level*level;
        health = 2*level*stamina + 80;
        Helper::repeatString(9,"\n");
        std::cout << "Congratulations " << name << "! You have levelled up to level " << level<< std::endl;
        std::cout << "Your health has increased to " << health<< std::endl;
        int points = 3;
        if(achievements.isEarned("Competent"))
            points++;
        if(achievements.isEarned("Powerful"))
            points++;
        if(achievements.isEarned("Thats A Big Mountain Lion"))
            points++;
        //this while runs while the character has points to spend and allows them to increase their stats
        while(points>0)
        {
            refreshStats();

            std::cout << std::endl;
            std::cout << "Select where to put your points, you have " << points << " to spend at the moment."<< std::endl;
            std::cout << "Your current stats are:"<< std::endl;
            std::cout << "Level: " << level << std::endl;
            std::cout << "Health: " << health << std::endl;
            std::cout << "Strength: " << coreStrength << " (" << strength << " total)" << std::endl;
            std::cout << "Stamina: " << coreStamina << " (" << stamina << " total)" << std::endl;
            std::cout << "Agility: " << coreAgility << " (" << agility << " total)" << std::endl;

            std::cout << "" << std::endl;
            std::cout << "1 - to deposit 1 point in strength; " << std::endl;
            std::cout << "this increases the damage you deal in combat." << std::endl;
            std::cout << "" << std::endl;
            std::cout << "2 - to deposit 1 point in stamina; " << std::endl;
            std::cout << "this increases your current health, how much you health gain when you level and the strength of potions." << std::endl;
            std::cout << "" << std::endl;
            std::cout << "3 - to deposit 1 point in agility; " << std::endl;
            std::cout << "this affects how often you attack in combat and your chance of a critical hit." << std::endl;
            std::cout << "" << std::endl;
            std::cout << "" << std::endl;

            //the user increasing their stat with an input of 1 2 or 3
            int i =  InputReader::getInt();
            if(i==1)
                coreStrength++;
            else if(i==2)
                coreStamina++;
            else if(i==3)
                coreAgility++;
            else
            {
                std::cout << "Please select where to place your points"<< std::endl;
                points++;
            }
            points--;
        }
        refreshStats();
        std::cout << "Your current stats are:"<< std::endl;
        std::cout << "Level: " << level << std::endl;
        std::cout << "Health: " << health << std::endl;
        std::cout << "Strength: " << coreStrength << " (" << strength << " total)" << std::endl;
        std::cout << "Stamina: " << coreStamina << " (" << stamina << " total)" << std::endl;
        std::cout << "Agility: " << coreAgility << " (" << agility << " total)" << std::endl;
    }
}

void Player::talents()
{
    std::cout << "Please select a talent"<< std::endl;
    std::cout << std::endl;
    if(!achievements.isEarned("Slayer I"))
    {
        if(achievements.isEarned("Strong"))
            std::cout << "1 - to get Slayer I (More Damage): increases damage by 10%"<< std::endl;
        else
            std::cout << "Slayer I requires You to have completed the Strong Achievement"<< std::endl;
    }
    else if(!achievements.isEarned("Slayer II"))
    {
        if(achievements.isEarned("Very Strong"))
            std::cout << "1 - to get Slayer II (Spiked Armour): weapon damage increased by 10% of total armour"<< std::endl;
        else
            std::cout << "Slayer II requires You to have completed the Very Strong Achievement"<< std::endl;
    }
    else if(!achievements.isEarned("Slayer III"))
    {
        if(achievements.isEarned("Super Strength"))
            std::cout << "1 - to get Slayer III (Massive Blow): Instantly kills a creature if you deal over 50% of its maximum health in a single hit"<< std::endl;
        else
            std::cout << "Slayer III requires You to have completed the Super Strength Achievement"<< std::endl;
    }
    else if(!achievements.isEarned("Slayer IV"))
    {
        if(achievements.isEarned("Mr Apollo"))
            std::cout << "1 - to get Slayer IV (Vampiric Strike): 10% of the damage you deal is returned to you as health"<< std::endl;
        else
            std::cout << "Slayer IV requires You to have completed the Mr Apollo Achievement"<< std::endl;
    }
    std::cout << std::endl;
    if(!achievements.isEarned("Tank I"))
    {
        if(achievements.isEarned("Healthy"))
            std::cout << "2 - to get Tank I (More Health): increases Health by 10%"<< std::endl;
        else
            std::cout << "Tank I requires You to have completed the Healthy Achievement"<< std::endl;
    }
    else if(!achievements.isEarned("Tank II"))
    {
        if(achievements.isEarned("Hard To Kill"))
            std::cout << "2 - to get Tank II (Powerful Potions): increases the power of potions by 100%"<< std::endl;
        else
            std::cout << "Tank II requires You to have completed the Hard To Kill Achievement"<< std::endl;
    }
    else if(!achievements.isEarned("Tank III"))
    {
        if(achievements.isEarned("Die Hard"))
            std::cout << "2 - to get Tank III (Sacrificial Strike): allows you to use 10% of your maximum health to deal 50% of your maximum health as armour ignoring damage"<< std::endl;
        else
            std::cout << "Tank III requires You to have completed the Die Hard Achievement"<< std::endl;
    }
    else if(!achievements.isEarned("Tank IV"))
    {
        if(achievements.isEarned("Tarrasque"))
            std::cout << "2 - to get Tank IV (Regeneration): you regenerate 10% of your health every round"<< std::endl;
        else
            std::cout << "Tank IV requires You to have completed the Tarrasque Achievement"<< std::endl;
    }
    std::cout << std::endl;
    if(!achievements.isEarned("Ninja I"))
    {
        if(achievements.isEarned("Quick"))
            std::cout << "3 - to get Ninja I (More Criticals): increases your critical chance by 10%"<< std::endl;
        else
            std::cout << "Ninja I requires You to have completed the Quick Achievement"<< std::endl;
    }
    else if(!achievements.isEarned("Ninja II"))
    {
        if(achievements.isEarned("Speedy"))
            std::cout << "3 - to get Ninja II (Deadly Criticals): doubles the effect agility has on criticals"<< std::endl;
        else
            std::cout << "Ninja II requires You to have completed the Speedy Achievement"<< std::endl;
    }
    else if(!achievements.isEarned("Ninja III"))
    {
        if(achievements.isEarned("Ninja"))
            std::cout << "3 - to get Ninja III (Sneak Attack): attacks you deal before you get hit are auto criticals and ignore armour"<< std::endl;
        else
            std::cout << "Ninja III requires You to have completed the Quick Achievement"<< std::endl;
    }
    else if(!achievements.isEarned("Ninja IV"))
    {
        if(achievements.isEarned("Time Traveller"))
            std::cout << "3 - to get Ninja IV (Ninja): Allows you to dodge your enemies attacks"<< std::endl;
        else
            std::cout << "Ninja IV requires You to have completed the Time Traveller Achievement"<< std::endl;
    }
    std::cout << std::endl;
    std::cout << "4 - to instead increase your Strength by 10"<< std::endl;
    std::cout << "5 - to instead increase your Stamina by 10"<< std::endl;
    std::cout << "6 - to instead increase your agility by 10"<< std::endl;

    int c = InputReader::getInt();

    switch(c)
    {
    case 1:
        if(achievements.isEarned("Slayer IV"))
            talents();
        else if(achievements.isEarned("Slayer III"))
        {
            if(achievements.isEarned("Mr Apollo"))
                achievements.earn("Slayer IV");
            else
                talents();
        }
        else if(achievements.isEarned("Slayer II"))
        {
            if(achievements.isEarned("Super Strength"))
                achievements.earn("Slayer III");
            else
                talents();
        }
        else if(achievements.isEarned("Slayer I"))
        {
            if(achievements.isEarned("Very Strong"))
                achievements.earn("Slayer II");
            else
                talents();
        }
        else
        {
            if(achievements.isEarned("Strong"))
                achievements.earn("Slayer I");
            else
                talents();
        }
        break;

    case 2:
        if(achievements.isEarned("Tank IV"))
            talents();
        else if(achievements.isEarned("Tank III"))
        {
            if(achievements.isEarned("Tarrasque"))
                achievements.earn("Tank IV");
            else
                talents();
        }
        else if(achievements.isEarned("Tank II"))
        {
            if(achievements.isEarned("Die Hard"))
                achievements.earn("Tank III");
            else
                talents();
        }
        else if(achievements.isEarned("Tank I"))
        {
            if(achievements.isEarned("Hard To Kill"))
                achievements.earn("Tank II");
            else
                talents();
        }
        else
        {
            if(achievements.isEarned("Healthy"))
                achievements.earn("Tank I");
            else
                talents();
        }
        break;

    case 3:
        if(achievements.isEarned("Ninja IV"))
            talents();
        else if(achievements.isEarned("Ninja III"))
        {
            if(achievements.isEarned("Time Traveller"))
                achievements.earn("Ninja IV");
            else
                talents();
        }
        else if(achievements.isEarned("Ninja II"))
        {
            if(achievements.isEarned("Ninja"))
                achievements.earn("Ninja III");
            else
                talents();
        }
        else if(achievements.isEarned("Ninja I"))
        {
            if(achievements.isEarned("Speedy"))
                achievements.earn("Ninja II");
            else
                talents();
        }
        else
        {
            if(achievements.isEarned("Quick"))
                achievements.earn("Ninja I");
            else
                talents();
        }
        break;

    case 4:
        coreStrength += 10;
        break;
    case 5:
        coreStamina += 10;
        break;
    case 6:
        coreAgility += 10;
        break;
    default:
        talents();
        break;
    }
}

void Player::refreshStats()
{
    strength = gearStrength + coreStrength;
    stamina = gearStamina + coreStamina;
    agility = gearAgility + coreAgility;
    health = getMaxHealth();
}

int Player::getMaxHealth()
{
    if(achievements.isEarned("Tank I"))
        return (int)((stamina*2*level+80)*1.1);
    return stamina*2*level+80;
}

void Player::orderItems()
{
    std::vector<Item*> temp; //create a temporary items list

    //first list the items that the character can sell eg unworn armour and weapons
    for(unsigned int i=0;i<items.size();i++)
    {
        if(items.at(i)->getWearing()||items.at(i)->getType()==7)
        {
            temp.push_back(items.at(i));
            items.erase(items.begin()+i);
            i--;
        }
    }

    //then list misc items (7)
    for(unsigned int i=0;i<temp.size();i++)
        items.push_back(temp.at(i));
    temp.clear();
}
Item * Player::itemInSlot(int type){
    switch(type)
            {
                case 1: return weapon;
                case 2: return body;
                case 3: return head;
                case 4: return shoulders;
                case 5: return hands;
                case 6: return legs;
    default: return NULL;
            }
}
void Player::earn(std::string name){
    achievementPoints += achievements.earn(name);
}

void Player::resetCharacterStats()
{
    //the level is initially 0 before the player goes through the level up method
    level = 0;

    //sets the experience to 0
    experience = 0;

    //sets the base health of the player as 40
    health = 40;

    //sets the required experience to level
    experienceRequired = 0;

    //sets the base stats all to 10
    coreStrength = 10;
    coreStamina = 10;
    coreAgility = 10;

    //sets the kills deaths and flees to 0 as the character hasn't done anything yet
    kills = 0;
    deaths = 0;
    flees = 0;

    //the character starts with 3 potions
    potions = 3;

    //the character starts with 0 time it has not been through any turns yet
    time = 0;

    //give the player 10 gold to start with
    gold = 10;

    //Initialises the ArrayList of items
    std::vector<Item*> temp;
    for(unsigned int i=0;i<items.size();i++)
        if(items.at(i)->equals("DM's heart",7)||items.at(i)->equals("God's heart",7))
            temp.push_back(items.at(i));
    items.clear();

    //adds the starting items to the Items ArrayList
    items.push_back(new Item("a stick", "stick",1,0,0,0,true,1,0,false));
    items.push_back(new Item("rags", "",0,0,0,0,true,2,0,false));
    items.push_back(new Item("thin hood", "",0,0,0,0,true,3,0,false));
    items.push_back(new Item("more rags", "",0,0,0,0,true,4,0,false));
    items.push_back(new Item("hobo gloves", "",0,0,0,0,true,5,0,false));
    items.push_back(new Item("loincloth", "",0,0,0,0,true,6,0,false));

    for(std::vector<Item*>::iterator it = temp.begin(); it!=temp.end();++it)
        items.push_back(*it);
    //get the stats on the starting gear
    gearStats();

    //get the total stats of the character
    totalStats();

    questType = 0;
    questTarget = 0;
    questProgress = 0;
    questGoal = 0;

    companion = 30;
    companionName = "Obo";
    passiveCompanion = false;

    getCreep("the GOD")->setEncounterStats(true, 0, 0, 0);

    do
    {
        std::cout << std::endl << std::endl;
        std::cout << "What difficulty do you wish to play?" << std::endl;
        std::cout << "1 - normal" << std::endl;
        std::cout << "normal" << std::endl;
        std::cout << "2 - Hard" << std::endl;
        std::cout << "200% ememy health" << std::endl;
        std::cout << "200% enemy damage" << std::endl;
        std::cout << "200% gold and experience gain" << std::endl;
        std::cout << "double starting money and potions" << std::endl;
        std::cout << "The abiltity to get hard achievements" << std::endl;
        difficulty = InputReader::getInt();
    }while(difficulty!=1&&difficulty!=2);
    if(difficulty==2)
    {
        potions += 3;
        gold += 10;
    }
}

double Player::critical()
{
    if(Helper::rd()+criticalChance()>1)
        return 1;
    return 0;
}
int Player::potionHeals()
{
    int healing = (int)(10*(stamina-9)*(1+Helper::rd()));

    if (achievements.isEarned("The Regenerator"))
        healing = (int)(healing*1.5);
    else if (achievements.isEarned("Well Stocked"))
        healing = (int)(healing*1.2);
    if(achievements.isEarned("Tank II"))
        healing *=2;
    return healing;
}

bool Player::equals(Player * p){
    if(p->health==health){
        health++;
        if(p->health==health){
            health--;
            return true;
        }
    }
    health--;
    return false;
}

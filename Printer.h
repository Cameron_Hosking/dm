#ifndef PRINTER_H
#define PRINTER_H
#include "player.h"
#include "Helper.h"
#include "item.h"

namespace printerHelper{

template<typename T>
void rec(std::stringstream & ss,T t){
    ss << t;
}

template<typename T, typename... Args>
void rec(std::stringstream & ss,T t, Args... args){
    ss << t;
    rec(ss,args...);
}
void print(std::stringstream &ss);
}

namespace Printer{
void printStartScreen();
void printMainMenu(Player * p);
void printMenu();
void achievementsMenu(int points);
void printEarnedPerks(Player * p);
void printUnearnedPerks(Player * p);

void townMenu(Player * p);
void printQuest(Player * p);
void printQuestInfo(Player * p);
void printQuestRewards(Player * p);
void printAllItems(std::vector<Item*> items);
void printInventoryStats(Player * p);
void printUpgradeOptions(int tier, Player * p);
void printItemStats(Item * a);
void printShop(Player * p);
void printMarket(Player * p);
void printArenaComplete(int time);
void printEpicArenaComplete(int time, int difficulty);
void itemCreationMessages(Player * p,int i);
void printCharacters(std::vector<std::string> names);
void printHelpContents();
void printHelpAbilityScores();
void printHelpFighting();
void printHelpInventory();
void printHelpQuests();
void printHelpEnchanting();
void printHelpTalents();
void printHelpCompanions();
void printHelpHints();

template<typename T, typename... Args>
void printer(T t, Args... args){
    std::stringstream ss;
    ss << t;
    printerHelper::rec(ss,args...);
    printerHelper::print(ss);
}

template<typename T>
void printer(T t){
    std::stringstream ss;
    ss << t;
    printerHelper::print(ss);
}



/**
 * @brief printColouredText
 * @return the empty string ""
 */
template <typename T>
std::string printColouredText(T content, ScrUtil::Color colour)
{
    ScrUtil::Attributes original = ScrUtil::getCurrentAttributes();
    std::cout.flush();
    ScrUtil::setColors(colour,original.paper);
    std::cout<<content;
    ScrUtil::setColors(original);

    return "";
}

}


#endif // PRINTER_H

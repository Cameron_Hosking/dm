#include "dm.h"


    Dm::Dm()
    {

    }

    /**
     * this is the main method from here we initiate the player then
     * we navigate to the mainscreen
     * @param arg
     */
    void Dm::runGame()
    {
        while(true)
        {
            bool characterLoaded = false;
            std::vector<std::string> names = getCharacters();
            std::string name;

            Printer::printStartScreen();
            std::cout << Helper::repeatString(6,"\n");
            std::cout << " 1 - Load Game   2 - New Game   3 - Help" << std::endl;
            switch(InputReader::getInt()){
            case 1:
                if(names.size()==0){
                    std::cout<<"there are no saved games"<<std::endl;
                    break;
                }
                std::cout<<"Enter the character name to load, type anything else to cancel"<<std::endl;
                Printer::printCharacters(names);
                name = InputReader::getString();
                if(Helper::contains(names,name)){
                    characterLoaded = true;
                }
                break;
            case 2:
                std::cout<<"What is your name?"<<std::endl;
                name = InputReader::getString();
                if(Helper::contains(names,name)){
                    std::cout<<name<<" already exists press enter to return to the main menu"<<std::endl;
                    InputReader::pressEnter();
                }
                else{
                    characterLoaded = true;
                }
                break;
            case 3:
                help();
                break;
            }
            if(!characterLoaded)
                continue;
            std::string dir = "saves/";
#ifdef S_IRWXG
            mkdir(dir.c_str(), S_IRWXU|S_IRWXG|S_IRWXO);
            dir.append(name);
            mkdir(dir.c_str(), S_IRWXU|S_IRWXG|S_IRWXO);
#else
            mkdir(dir.c_str());
            dir.append(name);
            mkdir(dir.c_str());
#endif
            p = new Player(name);
            if(p->marketItems.size()==0){
                createMarket();
            }
            p->save();
            mainScreen();
        }
    }

    void Dm::mainScreen()
    {
        //this loop runs until the player quits
        while(true)
        {
            //check if the player has earned any achievements
            p->earnAchievements();
            //level the player up if they have enough experience
            p->levelUp();
            //check again if the player has earned any achievements
            p->earnAchievements();
            //save the character
            companion = *p->getCreep(p->companion);
            p->save();
            //present the menu options, if true is returned then return;
            if(mainScreenOptions())
                return;
        }
    }

    void Dm::help()
    {
        while(true){
            Printer::printHelpContents();
            switch(InputReader::getInt()){
            case 0:
                return;
            case 1:
                Printer::printHelpAbilityScores();
                break;
            case 2:
                Printer::printHelpFighting();
                break;
            case 3:
                Printer::printHelpInventory();
                break;
            case 4:
                Printer::printHelpQuests();
                break;
            case 5:
                Printer::printHelpEnchanting();
                break;
            case 6:
                Printer::printHelpTalents();
                break;
            case 7:
                Printer::printHelpCompanions();
                break;
            case 8:
                Printer::printHelpHints();
                break;
            }
        }
    }

    bool Dm::mainScreenOptions()
    {
        Printer::printMainMenu(p);
        switch(InputReader::getInt()){
        case 0:
            std::cout << "goodbye" << std::endl;
            return true;
        case 1:
            menu();
            break;
        case 2:
            town();
            break;
        case 3:
            park();
            InputReader::pressEnter();
            break;
        case 4:
            regularForest();
            InputReader::pressEnter();
            break;
        case 5:
            deepForest();
            InputReader::pressEnter();
            break;
        case 6:
            mountain();
            InputReader::pressEnter();
            break;
        case 7:
            if(p->achievements.isEarned("New Champion"))
                desolateShores();
            InputReader::pressEnter();
            break;
        case 8:
            if(p->achievements.isEarned("New Champion"))
                islandOfLegends();
            InputReader::pressEnter();
            break;
        case 9:
            arena();
            InputReader::pressEnter();
            break;
        case 10:
            if(p->achievements.isEarned("New Champion"))
                epicArena();
            InputReader::pressEnter();
            break;
        case 11:
            pvp();
            InputReader::pressEnter();
            break;
        case 12:
            if(p->difficulty==2&&p->getCreep("the GOD")->getKills()>0)
                realmOfTheGods();
            InputReader::pressEnter();
            break;
        case 13:
            if(p->difficulty==2&&p->achievements.isEarned("Gotta Find Them All!"))
                ultimateShowdown();
            InputReader::pressEnter();
            break;
        case 1337:
            secretArea();
            InputReader::pressEnter();
            break;
        }
        return false;
    }

    void Dm::menu()
    {
        while(true)
        {
             Printer::printMenu();
            int choice = InputReader::getInt();
            if(choice==0)
                return;
            else if(choice==1)
                inventory();
            else if(choice==2)
                achievements();
            else if(choice==3)
                bestiary();
            else if(choice==4)
                questInfo();
            else if(choice==5)
                companionInfo();
        }
    }

    void Dm::inventory()
    {
        unsigned int selection = 1;
        while(selection!=0)
        {
            p->orderItems();
            Printer::printInventoryStats(p);
            std::cout << "enter the corresponding number to wear the item or to find out more about it" << std::endl;
            std::cout << "0 - to exit  999 - to sell or disenchant multiple" << std::endl;


            Printer::printAllItems(p->items);
            if(p->numberOfItem(WEAKSHARD,7)>0)
                std::cout << "weak shard x" << p->numberOfItem(WEAKSHARD,7) << std::endl;
            if(p->numberOfItem(SMALLSHARD,7)>0)
                std::cout << "small shard x" << p->numberOfItem(SMALLSHARD,7) << std::endl;
            if(p->numberOfItem(QUALITYSHARD,7)>0)
                std::cout << "quality shard x" << p->numberOfItem(QUALITYSHARD,7) << std::endl;
            if(p->numberOfItem(BRILLIANTSHARD,7)>0)
                std::cout << "brilliant shard x" << p->numberOfItem(BRILLIANTSHARD,7) << std::endl;
            if(p->numberOfItem(SUPERIORSHARD,7)>0)
                std::cout << "superior shard x" << p->numberOfItem(SUPERIORSHARD,7) << std::endl;
            if(p->numberOfItem(EPICSHARD,7)>0)
                std::cout << "epic shard x" << p->numberOfItem(EPICSHARD,7) << std::endl;
            if(p->numberOfItem(LEGENDARYSHARD,7)>0)
                std::cout << "legendary shard x" << p->numberOfItem(LEGENDARYSHARD,7) << std::endl;
            if(p->numberOfItem(DRAGONSHARD,7)>0)
                std::cout << "dragon shard x" << p->numberOfItem(DRAGONSHARD,7) << std::endl;
            selection = InputReader::getInt();

            if(selection==999)
            {
                std::cout << "0 - to cancel" << std::endl;
                std::cout << "1 - to sell all items (except for shards, miscellania, enchanted and worn) that are not epic or better" << std::endl;
                std::cout << "2 - to sell all items (except for shards, miscellania, enchanted and worn) that are not legendary " << std::endl;
                std::cout << "3 - to disenchant all items (except for shards, miscellania, enchanted, worn and non magical) that are not epic or better" << std::endl;
                std::cout << "4 - to disenchant all items (except for shards, miscellania, enchanted, worn and non magical) that are not legendary " << std::endl;
                std::cout << "5 - to disenchant all items (except for shards, miscellania, enchanted, worn and non magical)" << std::endl;
                int selling = InputReader::getInt();
                if(selling==0)
                    break;
                else if(selling==1||selling==2)
                {
                    int goldInitial = p->gold;
                    int sold = 0;
                    for(unsigned int i = 0;i<p->items.size();i++)
                    {
                        Item * a = p->items.at(i);
                        if(Helper::startsWith(a->getName(),"Epic")&&selling==1);
                        else if(!a->getWearing()&&a->getType()!=7&&!a->getAugmented()&&!Helper::startsWith(a->getName(),"Legendary"))
                        {
                            sold++;
                            sell(i);
                            i--;
                        }

                    }
                    int moneyMade = p->gold - goldInitial;
                    std::cout << "You have sold " << sold << " items for " << moneyMade << "gp" << std::endl;
                }
                else if(selling==3||selling==4||selling==5)
                {
                    for(unsigned int i = 0;i<p->items.size();i++)
                    {
                        Item * a = p->items.at(i);
                        if((Helper::startsWith(a->getName(),"Epic")&&selling==3)||a->getAugmented());
                        else if(Helper::startsWith(a->getName(),"Legendary")&&(selling==3||selling==4));
                        else if(!a->getWearing()&&a->getType()!=7&&a->getStrength()+a->getStamina()+a->getAgility()>0)
                        {
                            disenchant(i);
                            i--;
                        }
                    }
                }
            }
            if(selection>p->items.size()||selection==0)
                break;
            Item * a = p->items.at(selection-1);
            if(a->getType()!=7&&!a->getWearing())
            {
                Item * itemWorn = p->itemInSlot(a->getType());
                std::cout << "Inventory item: " << std::endl;
                 Printer::printItemStats(a);
                std::cout << std::endl;
                std::cout << "Worn item: " << std::endl;
                 Printer::printItemStats(itemWorn);
                std::cout << std::endl;
                if(a->getStrength()+a->getStamina()+a->getAgility()>0&&!a->getWearing())
                    std::cout << "1 - to wear the item, 2 - to sell for " << a->getValue() << "gp, 3 - to disenchant it, 0 - to cancel" << std::endl;
                else
                    std::cout << "1 - to wear the item, 2 - to sell for " << a->getValue() << "gp, 0 - to cancel" << std::endl;
                int choice = InputReader::getInt();
                if(choice==1)
                {
                    a->setWearing(true);
                    itemWorn->setWearing(false);
                }
                else if(choice==2)
                    sell(selection-1);
                else if(choice==3&&a->getStrength()+a->getStamina()+a->getAgility()>0&&!a->getWearing())
                    disenchant(selection-1);
            }
        }
    }

    /**
     * the achievements method allows the user to look at their earned and unearned achievements
     */
    void Dm::achievements()
    {
        int exit = -1;
        while(exit!=0)
        {
            Printer::achievementsMenu(p->achievementPoints);
            exit = InputReader::getInt();
            if(exit==1)
                p->achievements.printEarned();
            else if(exit==2)
                p->achievements.printUnearned();
            else if(exit==3)
                Printer::printEarnedPerks(p);
            else if(exit==4)
                Printer::printUnearnedPerks(p);
        }
    }

    /**
     * prints the stats of all the creatures the player has fought
     */
    void Dm::bestiary()
    {
        p->printCreepStats();
        std::cout << "Total Kills: " << p->kills << std::endl;
        std::cout << "Total Deaths: " << p->deaths << std::endl;
        std::cout << "Total Flees: " << p->flees << std::endl;
        std::cout << std::endl;
    }

    void Dm::questInfo()
    {
        if(p->questPoints==1)
            std::cout << "You have completed " << p->questPoints << " Quest so far" << std::endl;
        else
            std::cout << "You have completed " << p->questPoints << " Quests so far" << std::endl;
        if(p->questType==0||p->questType==-1)
        {
            std::cout << "you are not currently on a quest" << std::endl;
            return;
        }

         Printer::printQuestInfo(p);
         Printer::printQuestRewards(p);

        std::cout << "1 - to continue" << std::endl;
        std::cout << "2 - to abandon quest, you will not be able to pick up another quest this turn" << std::endl;
        if(InputReader::getInt()==2)
        {
            resetQuest();
            p->questType = -1;
        }
    }

    void Dm::companionInfo()
    {
        int choice = 0;
        while(choice!=1)
        {
            std::cout << p->companionName << " " << companion.getName() << std::endl;
            std::cout << "Power: " << companion.getDamage() << std::endl;
            std::cout << "Speed: " << companion.getAgility() << std::endl;
            std::cout << std::endl;
            std::cout << "1 - to continue 2 - to rename your companion 3 - to toggle passive mode" << std::endl;
            std::cout << "Your pet is currently: ";
            if(p->passiveCompanion)
                std::cout << "passive" << std::endl;
            if(!p->passiveCompanion)
                std::cout << "agressive" << std::endl;
            choice = InputReader::getInt();
            if(choice==2)
            {
                std::cout << "What is the new name of your companion?" << std::endl;
                p->companionName = InputReader::getString();
                std::cout << "Your companions new name is " << p->companionName << " " << companion.getName() << std::endl;
            }
            if(choice==3)
                p->passiveCompanion = !p->passiveCompanion;
        }

    }

    /**
     * lets the player choose what part of the town to go to and runs the relevant method
     */
    void Dm::town()
    {
        while(true){
            Printer::townMenu(p);
            switch(InputReader::getInt()){
                case 0:
                    return;
                case 1:
                    mayor();
                    InputReader::pressEnter();
                    break;
                case 2:
                    shop();
                    break;
                case 3:
                    enchant();
                    break;
                case 4:
                    if(p->achievements.isEarned("The Value Of Coin"))
                        market();
                    break;
                case 5:
                    if(p->achievements.isEarned("New Champion"))
                        playerCreateItem();
                    break;
            }
        }
    }

      void Dm::mayor()
    {
        if(p->level>=60)
        {
            std::cout << "Everything I know of should be a joke for you now, no more quests for you" << std::endl;
            //InputReader::pressEnter();
            return;
        }
        if(p->questType==-1)
        {
            std::cout << "You spurned my quest, get out of my office." << std::endl;
        }
        else if(p->questType==0)
        {
            if(Helper::rd()>0.2||!p->achievements.isEarned("Triple Figures"))
            {
                int c = 0;
                if(p->level<50)
                {
                    std::cout << "Are you a man or a mouse?" << std::endl;
                    while(c!=1&&c!=2)
                    {
                        std::cout << " 1 - man   2 - mouse" << std::endl;
                        c = InputReader::getInt();
                    }
                }
                else
                    c = 2;
                p->questType = 1;
                int variance = 4 + (int)(p->level/10);
                if (c==2)
                    variance = - 5 - (int)(p->level/10);

                while(true)
                {
                    p->questTarget = (int)(Helper::rd()*variance + p->level/2 + 4);
                    if(p->questTarget<30&&p->questTarget>=0)
                        break;
                }
                p->questGoal = (int)(Helper::rd()*4+1);
            }
            else
            {
                p->questType=2;
                int tier = 0;
                while(tier>7||tier<=0)
                    tier = (int)((Helper::rd()*p->level/6)+1);
                p->questGoal = (int)(Helper::rd()*3+1);
                if(tier>=1)
                    p->questTarget++;
                if(p->questTarget>=2&&p->achievements.isEarned("Achiever"))
                    p->questTarget++;
                if(p->questTarget>=3&&p->achievements.isEarned("Good Achiever"))
                    p->questTarget++;
                if(p->questTarget>=4&&p->achievements.isEarned("Bronze Achiever"))
                    p->questTarget++;
                if(p->questTarget>=5&&p->achievements.isEarned("Silver Achiever"))
                    p->questTarget++;
                if(p->questTarget>=6&&p->achievements.isEarned("Gold Achiever"))
                    p->questTarget++;
                if(p->questTarget>=7&&p->achievements.isEarned("Platinum Achiever"))
                    p->questTarget++;
            }
             Printer::printQuest(p);
        }
        else
            std::cout << "your already on a quest come back when you've finished." << std::endl;
            //InputReader::pressEnter();
    }

    /**
     * allows the  player to purchase items and potions
     */
    void Dm::shop()
    {
        Printer::printShop(p);
        while(true)
        {
            int b =  InputReader::getInt();
            int cost = 0;
            Item * item;
            switch(b)
            {
                case 0:
                    return;
                case 1:
                    cost = 20;
                    item = new Item("shortsword", "jab with the shortsword at",4,0,0,0,false,1,10,false);
                    break;
                case 2:
                    cost = 100;
                    item = new Item("longsword", "swing the longsword at",8,0,0,0,false,1,50,false);
                    break;
                case 3:
                    cost = 500;
                    item = new Item("waraxe", "hack",12,0,0,0,false,1,200,false);
                    break;
                case 4:
                    cost = 2000;
                    item = new Item("claymore", "cleave",20,0,0,0,false,1,500,false);
                    break;
                case 5:
                    cost = 8000;
                    item = new Item("executioners axe", "attempt to execute",50,0,0,0,false,1,2000,false);
                    break;
                case 6:
                    cost = 25000;
                    item = new Item("Death's Scythe","harvest the soul of",100,0,0,0,false,1,10000,false);
                    break;
                case 7:
                    cost = 20;
                    item = new Item("leather armour", "",5,0,0,0,false,2,10,false);
                    break;
                case 8:
                    cost = 100;
                    item = new Item("chainmail", "",10,0,0,0,false,2,50,false);
                    break;
                case 9:
                    cost = 500;
                    item = new Item("breastplate", "",20,0,0,0,false,2,200,false);
                    break;
                case 10:
                    cost = 2000;
                    item = new Item("scale mail", "",50,0,0,0,false,2,1000,false);
                    break;
                case 11:
                    cost = 5;
                    item = new Item("skull cap", "",1,0,0,0,false,3,2,false);
                    break;
                case 12:
                    cost = 20;
                    item = new Item("leather hood", "",2,0,0,0,false,3,10,false);
                    break;
                case 13:
                    cost = 100;
                    item = new Item("steel hat", "",4,0,0,0,false,3,50,false);
                    break;
                case 14:
                    cost = 500;
                    item = new Item("fullhelm", "",10,0,0,0,false,3,200,false);
                    break;
                case 15:
                    cost = 5;
                    item = new Item("cloth shoulderpads", "",1,0,0,0,false,4,2,false);
                    break;
                case 16:
                    cost = 20;
                    item = new Item("leather shoulderpads", "",2,0,0,0,false,4,10,false);
                    break;
                case 17:
                    cost = 100;
                    item = new Item("steel pauldrons", "",4,0,0,0,false,4,50,false);
                    break;
                case 18:
                    cost = 500;
                    item = new Item("oversized pauldrons", "",10,0,0,0,false,4,200,false);
                    break;
                case 19:
                    cost = 5;
                    item = new Item("silk gloves", "",1,0,0,0,false,5,2,false);
                    break;
                case 20:
                    cost = 20;
                    item = new Item("leather gloves", "",2,0,0,0,false,5,10,false);
                    break;
                case 21:
                    cost = 100;
                    item = new Item("steel gauntlets", "",4,0,0,0,false,5,50,false);
                    break;
                case 22:
                    cost = 500;
                    item = new Item("spiked gauntlets", "",10,0,0,0,false,5,200,false);
                    break;
                case 23:
                    cost = 20;
                    item = new Item("snazzy pants", "",5,0,0,0,false,6,10,false);
                    break;
                case 24:
                    cost = 100;
                    item = new Item("leather chaps", "",10,0,0,0,false,6,50,false);
                    break;
                case 25:
                    cost = 500;
                    item = new Item("chain leggings", "",20,0,0,0,false,6,200,false);
                    break;
                case 26:
                    cost = 2000;
                    item = new Item("steel platelegs", "",50,0,0,0,false,6,1000,false);
                    break;
                case 27:
                    {
                        std::cout << "How many potions do you wish to buy?" << std::endl;
                        int numberOfPotions = InputReader::getInt();
                        int i = 0;
                        while(i<numberOfPotions)
                        {
                            if(p->gold>=p->scalingCost())
                            {
                                p->earn("Tasty");
                                p->potions++;
                                p->gold -= p->scalingCost();
                            }
                            else
                            {
                                std::cout << "you could not afford that many potions" << std::endl;
                                break;
                            }
                            i++;
                        }
                         std::cout << "you have purchased " << i << " potions" << std::endl;
                    }
                    break;
                default:
                    std::cout << "That is not an option" << std::endl;
            }
            if(cost!=0)
            {
                if(cost>p->gold){
                    Printer::printer(item->getColouredName() , " costs " , cost , "gp, you only have " , p->gold , "gp\n");
                    delete(item);
                }
                else
                {
                    p->earn("The Value Of Coin");
                    if(item->equals("Death's Scythe",1))
                        p->earn("Deadly Spender");
                    if(item->equals("snazzy pants",6))
                        p->earn("Money Can Buy Happiness");
                    p->items.push_back(item);
                    p->gold -= cost;
                    Printer::printer("you have purchased " , item->getColouredName() , " for " , cost , "gp, go to your inventory to equip this item\n");
                }
            }
            std::cout << std::endl;
            std::cout << "Enter a number to buy the corresponding item." << std::endl;
            std::cout << "You have " << p->gold << " gold to spend" << std::endl;
            std::cout << "0 - to exit" << std::endl;
        }

    }

    void Dm::enchant()
    {
        while(true)
        {
            std::cout << Helper::repeatString(4,"\n");
            std::cout << "Select an item that you wish to enchant" << std::endl;
            std::cout << "0 - to exit" << std::endl;
            printItems();
            unsigned int selection = InputReader::getInt();
            if(selection>p->items.size()||selection==0)
                return;
            Item * b = p->items.at(selection-1);
            if(b->getType()!=7)
            {
                if(b->getAugmented()&&!Helper::startsWith(b->getName(),"+"))
                {
                    std::cout << "Dragonic items cannot become any more powerful" << std::endl;
                    break;
                }
                if(!b->getAugmented())
                {
                    if(p->achievements.isEarned("Triple Figures"))
                        preUpgradeProcess(b,1);
                    else
                        std::cout << "you are not yet able to upgrade this item, you need Triple Figures" << std::endl;
                }
                if(Helper::startsWith(b->getName(),"+1"))
                {
                    if(p->achievements.isEarned("Achiever"))
                        preUpgradeProcess(b,2);
                    else
                        std::cout << "you are not yet able to upgrade this item any further, you need Achiever" << std::endl;
                }
                if(Helper::startsWith(b->getName(),"+2"))
                {
                    if(p->achievements.isEarned("Good Achiever"))
                        preUpgradeProcess(b,3);
                    else
                        std::cout << "you are not yet able to upgrade this item any further, you need Good Achiever" << std::endl;
                }
                if(Helper::startsWith(b->getName(),"+3"))
                {
                    if(p->achievements.isEarned("Bronze Achiever"))
                        preUpgradeProcess(b,4);
                    else
                        std::cout << "you are not yet able to upgrade this item any further, you need Bronze Achiever" << std::endl;
                }
                if(Helper::startsWith(b->getName(),"+4"))
                {
                    if(p->achievements.isEarned("Silver Achiever"))
                        preUpgradeProcess(b,5);
                    else
                        std::cout << "you are not yet able to upgrade this item any further, you need Silver Achiever" << std::endl;
                }
                if(Helper::startsWith(b->getName(),"+5"))
                {
                    if(p->achievements.isEarned("Gold Achiever"))
                        preUpgradeProcess(b,6);
                    else
                        std::cout << "you are not yet able to upgrade this item any further, you need Gold Achiever" << std::endl;
                }
                if(Helper::startsWith(b->getName(),"+6"))
                {
                    if(p->achievements.isEarned("Platinum Achiever"))
                        preUpgradeProcess(b,7);
                    else
                        std::cout << "you are not yet able to upgrade this item, you need Platinum Achiever" << std::endl;
                }
                if(Helper::startsWith(b->getName(),"+7"))
                    std::cout << "you cannot upgrade an item past +7" << std::endl;
            }
            else
                std::cout << "you cannot enchant this item" << std::endl;
            if(selection==0)
                break;
        }
    }

    void Dm::market()
    {

        Printer::printMarket(p);
        unsigned int c = InputReader::getInt();
        while(c!=0)
        {
            if(c<=p->marketItems.size()&&c>0)
            {
                Item * marketItem = p->marketItems.at(c-1);
                if(marketItem->getType()==7)
                    std::cout << "This is a " << marketItem->getName() << " used for enchanting" << std::endl;
                else{
                    Item * current = p->itemInSlot(marketItem->getType());
                    std::cout << "Market item: " << std::endl;
                    Printer::printItemStats(marketItem);
                    std::cout << std::endl;
                    std::cout << "Your item: " << std::endl;
                    Printer::printItemStats(current);
                }
                int buy = -1;
                while(buy!=0)
                {
                    std::cout << std::endl;
                    std::cout << "1 - to buy for " << marketItem->getValue()*2 << "gp, 0 - to cancel" << std::endl;
                    buy = InputReader::getInt();
                    if(buy==1){
                        if(p->gold>=(marketItem->getValue()*2))
                        {
                            p->gold -= marketItem->getValue()*2;
                            p->items.push_back(p->marketItems.at(c-1));
                            p->marketItems.erase(p->marketItems.begin()+c-1);
                        }
                        else
                            std::cout << "you cannot afford that" << std::endl;
                        break;
                    }
                }
            }
            else
                std::cout << "that is not a valid choice" << std::endl;
            Printer::printMarket(p);
            c = InputReader::getInt();
        }
    }

    void Dm::park()
    {
        incrementTurn();
        std::vector<Creep*> creeps;
        for(int i=0; i<(Helper::rd()*3)&&i<(p->level+1)/2;i++)
        {
            int f = (int)(Helper::rd()*5);
            if(f<2)
                creeps.push_back(p->getCreep("the weak test subject"));
            else if(f==2)
                creeps.push_back(p->getCreep("the furious bunny"));
            else if (f==3)
                creeps.push_back(p->getCreep("the average joe"));
            else
                creeps.push_back(p->getCreep("the overly friendly dog"));
        }
        result(fight(creeps),creeps);
    }

      void Dm::regularForest()
    {
        if(p->level<4&&!p->achievements.isEarned("The God Slayer"))
        {
            std::cout << "are you sure you want to continue? you are advised to be at least level 4 before entering the forest" << std::endl;
            std::cout << "1 - to continue, 2 - to leave" << std::endl;
            if(InputReader::getInt()!=1)
                return;
        }
        incrementTurn();
        std::vector<Creep*> creeps;
        for(int i=0; i<(Helper::rd()*3);i++)
        {
            int f = (int)(Helper::rd()*5);
            if (f==0)
                creeps.push_back(p->getCreep("the big pig"));
            else if(f==1)
                creeps.push_back(p->getCreep("the training dummy"));
            else if(f==2)
                creeps.push_back(p->getCreep("the agile test subject"));
            else if (f==3)
                creeps.push_back(p->getCreep("the naked goblin"));
            else if (f==4)
                creeps.push_back(p->getCreep("the strong test subject"));
        }
        result(fight(creeps),creeps);

    }

    void Dm::deepForest()
    {
        if(p->level<8&&!p->achievements.isEarned("The God Slayer"))
        {
            std::cout << "are you sure you want to continue? you are advised to be at least level 8 before entering the deeper forest" << std::endl;
            std::cout << "1 - to continue, 2 - to leave" << std::endl;
            if(InputReader::getInt()!=1)
                return;
        }
        incrementTurn();
        std::vector<Creep*> creeps;
        for(int i=0; i<(Helper::rd()*3);i++)
        {
            int f = (int)(Helper::rd()*4);
            if (f==0)
                creeps.push_back(p->getCreep("the goblin soldier"));
            else if (f==1)
                creeps.push_back(p->getCreep("the treeman"));
            else if (f==2)
                creeps.push_back(p->getCreep("the floating fist"));
            else
                creeps.push_back(p->getCreep("the random angry monkey"));
        }
        result(fight(creeps),creeps);
    }

      void Dm::mountain()
    {
        if(p->level<12&&!p->achievements.isEarned("The God Slayer"))
        {
            std::cout << "are you sure you want to continue? you are advised to be at least level 12 before entering the mountains" << std::endl;
            std::cout << "1 - to continue,  2 - to leave" << std::endl;
            if(InputReader::getInt()!=1)
                return;
        }
        incrementTurn();
        std::vector<Creep*> creeps;
        for(int i=0; i<(Helper::rd()*3);i++)
        {
            int f = (int)(Helper::rd()*5.2);
            if (f==0)
                creeps.push_back(p->getCreep("the blob"));
            else if(f==1)
                creeps.push_back(p->getCreep("the grizzly bear"));
            else if(f==2)
                creeps.push_back(p->getCreep("the doppelganger"));
            else if (f==3)
                creeps.push_back(p->getCreep("the quiet ninja"));
            else if (f==4)
                creeps.push_back(p->getCreep("the rock golem"));
            else
                creeps.push_back(p->getCreep("the ticking easter egg"));
        }
        result(fight(creeps),creeps);
    }

      void Dm::desolateShores()
      {
          incrementTurn();
          std::vector<Creep*> creeps;
          for(int i=0; i<(Helper::rd()*2.5);i++)
          {
              int f = (int)(Helper::rd()*5);
              if (f==0)
                  creeps.push_back(p->getCreep("the green dragon"));
              else if(f==1)
                  creeps.push_back(p->getCreep("the undead arena champion"));
              else if(f==2)
                  creeps.push_back(p->getCreep("the cyclops"));
              else if (f==3)
                  creeps.push_back(p->getCreep("the lich"));
              else if (f==4)
                  creeps.push_back(p->getCreep("the lemon"));
          }
          result(fight(creeps),creeps);
      }

    void Dm::islandOfLegends()
    {
        incrementTurn();
        std::vector<Creep*> creeps;
        for(int i=0; i<(Helper::rd()*3);i++)
        {
            int f = (int)(Helper::rd()*5);
            if (f==0)
                creeps.push_back(p->getCreep("the gamma ray"));
            else if (f==1)
                creeps.push_back(p->getCreep("the vampire lord"));
            else if (f==2)
                creeps.push_back(p->getCreep("the minotaur"));
            else if (f==3)
                creeps.push_back(p->getCreep("the diamond golem"));
            else
                creeps.push_back(p->getCreep("the black dragon"));
        }
        result(fight(creeps),creeps);
    }

      void Dm::realmOfTheGods(){
        incrementTurn();
        std::vector<Creep*> creeps;
        for(int i=0; i<(Helper::rd()*1.2);i++)
        {
            int f = (int)(Helper::rd()*6);
            if (f==0)
                creeps.push_back(p->getCreep("The giant flying spaghetti monster"));
            else if(f==1)
                creeps.push_back(p->getCreep("Buddha"));
            else if(f==2)
                creeps.push_back(p->getCreep("Hades"));
            else if (f==3)
                creeps.push_back(p->getCreep("Bruce Lee"));
            else if (f==4)
                creeps.push_back(p->getCreep("Chuck Norris"));
            else
                creeps.push_back(p->getCreep("Zeus"));
        }
        result(fight(creeps),creeps);
    }

    void Dm::arena()
    {
        if(p->level>74)
        {
            std::cout << "You are too powerful to enter this arena" << std::endl;
            return;
        }
        if(p->gold<p->scalingCost())
        {
            std::cout << "You do not have enough money to enter the arena" << std::endl;
            return;
        }
        incrementTurn();
        p->gold-=p->scalingCost();
        int i = (int)((p->level/3)-5);
        if(i<0)
            i=0;
        else
            std::cout << "because you are level " << p->level << " you start at round " << (i+1) << std::endl;
        while(true)
        {
            std::cout << "Round " << (i+1) << " has Begun!" << std::endl;
            std::vector<Creep*> creeps;
            if(i!=18)
                creeps.push_back(p->getMegaCreep(i));
            else{
                creeps.push_back(p->getMegaCreep(14));
                creeps.push_back(p->getMegaCreep(16));
                creeps.push_back(p->getMegaCreep(17));
            }
            int result = fight(creeps);
            Dm::result(result,creeps);
            if(result==1)
            {
                if(creeps.at(0)->equals("the arena champion"))
                {
                    if(p->health==p->getMaxHealth())
                        p->earn("Untouchable");
                    p->earn("New Champion");
                    if(p->time<=150)
                        p->earn("Down To Business");
                    if(p->time<=100)
                        p->earn("Quick Kill");
                    if(p->time<=60)
                        p->earn("Record Kill");
                    if(p->deaths==0)
                        p->earn("Invincible");
                    if(p->flees==0)
                        p->earn("Brave");
                    if(p->flees==0&&p->deaths==0&&p->time<=100&&p->difficulty==2)
                        p->earn("Absolute Perfection");
                     Printer::printArenaComplete(p->time);
                    break;
                }


                else
                {
                    p->gold-=creeps.at(0)->getGp();
                    int c = 0;
                    while(c!=2&&c!=1)
                    {
                        std::cout << "1 - to continue on to round " << (i+2) << ", 2 - to retire." << std::endl;
                        std::cout << Helper::repeatString(4,"\n");
                        c =  InputReader::getInt();


                    }
                    if(c==1)
                        i++;
                    else
                    {
                        getRandomItem((2*(i+1)));
                        break;
                    }

                }
            }
            else
                break;
        }
    }

    void Dm::epicArena()
    {
        if(p->gold<2*p->scalingCost())
        {
            std::cout << "You do not have enough money to enter the arena" << std::endl;
            return;
        }
        incrementTurn();
        p->gold-=2*p->scalingCost();
        int i = 0;
        while(true)
        {
            std::cout << "Round " << (i+1) << " has Begun!" << std::endl;
            std::vector<Creep*> creeps;
            creeps.push_back(p->getMegaCreep(i+21));
            if(creeps.at(0)->equals("the GOD")&&creeps.at(0)->getKills()>=3)
            {
                if(p->difficulty==1)
                {
                    p->gold+=2*p->scalingCost();
                    std::cout << "You can get no further in normal mode, restart to play in hard mode" << std::endl;
                    std::cout << "(you have been refunded your arena fee)" << std::endl;
                    std::cout << "1 - to reset character, 2 - to continue" << std::endl;
                    int c = InputReader::getInt();
                    if(c==1)
                    {
                        p->items.push_back(new Item("God's heart"," ",0,0,0,0,false,7,0,false));
                        p->resetCharacterStats();
                    }
                    break;
                }
                else
                {
                    std::cout << "There is nothing remaining to do in the epic arena, explore the realm of gods to continue. 5 - to restart the game or anything else to cancel" << std::endl;
                    if(InputReader::getInt()==5)
                    {
                        p->items.push_back(new Item("God's heart"," ",0,0,0,0,false,7,0,false));
                        p->resetCharacterStats();
                    }
                    break;

                }
            }
            int result = fight(creeps);
            Dm::result(result,creeps);
            if(result==1)
            {
                if(creeps.at(0)->equals("the GOD"))
                {

                    p->achievements.earn("The God Slayer");
                    if(p->level<50&&p->difficulty==2)
                        p->achievements.earn("Underdog");
                     Printer::printEpicArenaComplete(p->time,p->difficulty);

                    int c = InputReader::getInt();
                    if(c==1)
                    {
                        p->resetCharacterStats();
                        p->items.push_back(new Item("Gods heart"," ",0,0,0,0,false,7,0,false));
                    }
                    else
                        getRandomItem(59);
                    break;
                }

                else
                {
                    p->gold-=creeps.at(0)->getGp();
                    std::cout << "1 - to continue on to round " << (i+2) << ", 2 - to retire." << std::endl;
                    std::cout << Helper::repeatString(4,"\n");
                    int c =  InputReader::getInt();
                    if(c==1)
                        i++;
                    else
                    {
                        if (i>5)
                            getRandomItem(59);
                        else
                            getRandomItem(49);
                        break;
                    }
                }
            }
            else
                break;
        }
    }

    void Dm::ultimateShowdown()
    {
        incrementTurn();
        for(int i=0;i<6;i++)
        {
            std::vector<Creep*> creeps;
            creeps.push_back(p->getCreep(i+31));
            int result = fight(creeps);
            Dm::result(result,creeps);
            if(result!=1)
                break;
        }
        std::vector<Creep*> creeps;
        Creep * c = new Creep("the DM");
        c->initilize(4000000, 15000, 1000, "gives you a reality check", "armour of reality", 100000, 0, 0, 0, 0);
        creeps.push_back(c);
        std::cout << "You thought you'd be killing yet another one of my gods well guess what, playtimes over " << p->name << ", time to meet your maker" << std::endl;
        int result = fight(creeps);
        Dm::result(result,creeps);
        delete(c);
        if(result==1)
        {
            std::cout << "I'll have the last laugh yet... FORCED CHARACTER RESET." << std::endl;
            p->achievements.earn("????");
            p->items.push_back(new Item("DM's heart"," ",0,0,0,0,false,7,0,false));
            p->resetCharacterStats();

        }
    }

    void Dm::secretArea()
    {
        if(!p->achievements.isEarned("DM")||p->numberOfItem(DRAGONSHARD,7)>0)
        {
            std::cout << "You need to have earned the DM achievement and have a dragon shard to enter the secret area" << std::endl;
            return;
        }
        std::cout << "*******Secret Area*********" << std::endl;
        std::cout << std::endl;
        std::cout << "A mysterious old man greets you, he offers access to a secret challenge arena and elite equipment." << std::endl;
        std::cout << "1 - to enter the dragon shop" << std::endl;
        std::cout << "2 - to enter the challenge arena" << std::endl;
        std::cout << "0 - to exit" << std::endl;
        int c = InputReader::getInt();
        if(c==1)
            dragonShop();
        else if(c==2)
            megaBattle();

    }

      void Dm::dragonShop(){
        std::cout << "All armour costs 1 dragon shard and 10 million gp" << std::endl;
        std::cout << "Weapons cost 2 dragon shards and 20 million gp" << std::endl;
        std::cout << "Dragon equipment cannot be enchanted\n" << std::endl;
        std::cout << "1 - to view weapons" << std::endl;
        std::cout << "2 - to view body armour" << std::endl;
        std::cout << "3 - to view head armour" << std::endl;
        std::cout << "4 - to view shoulder armour" << std::endl;
        std::cout << "5 - to view hand armour" << std::endl;
        std::cout << "6 - to view leg armour" << std::endl;
        std::cout << "0 - to exit" << std::endl;
        int type = InputReader::getInt();
        std::string name;
        std::string a;
        int cost = 1;
        switch(type)
        {
            case 1: name = "bane";cost++;break;
            case 2: name = " full plate";break;
            case 3: name = " full helm";break;
            case 4: name = " pouldrens";break;
            case 5: name = " guantlets";break;
            case 6: name = " platelegs";break;
            default: return;
        }
        name = "Dragon"+name;
        if(type==1)
            a= "Attack:";
        else
            a = "Armour:";
        std::cout << "Which item do you want?" << std::endl;

        std::cout << "1 - for " << name << " of strength" << std::endl;
        std::cout << a << "500	Str:120	Sta:0	Agi:0";
        std::cout << std::endl;
        std::cout << "2 - for " << name << " of stamina" << std::endl;
        std::cout << a << "500	Str:0	Sta:120	Agi:0";
        std::cout << std::endl;
        std::cout << "3 - for " << name << " of agility" << std::endl;
        std::cout << a << "500	Str:0	Sta:0	Agi:120";
        std::cout << std::endl;
        std::cout << "4 - for " << name << " of might" << std::endl;
        std::cout << a << "500	Str:65	Sta:65	Agi:0";
        std::cout << std::endl;
        std::cout << "5 - for " << name << " of slaying" << std::endl;
        std::cout << a << "500	Str:65	Sta:0	Agi:65";
        std::cout << std::endl;
        std::cout << "6 - for " << name << " of tenacity" << std::endl;
        std::cout << a << "500	Str:0	Sta:65	Agi:65";
        std::cout << std::endl;
        std::cout << "7 - for " << name << " of the warlord" << std::endl;
        std::cout << a << "500	Str:45	Sta:45	Agi:45";
        std::cout << std::endl;
        std::cout << "0 - to exit" << std::endl;
        int stats = InputReader::getInt();
        if(stats<1||stats>7)
            return;
        if(cost*10000000>p->gold||cost>p->numberOfItem(DRAGONSHARD,7))
        {
            std::cout << "You cannot afford this" << std::endl;
            return;
        }
        p->removeItems(DRAGONSHARD,7,cost);
        p->gold = p->gold - cost*10000000;
        int str = 0;
        int sta = 0;
        int agi = 0;
        switch(stats)
        {
        case 1: name = name+" of strength";str = 120;break;
        case 2: name = name+" of stamina";sta = 120;break;
        case 3: name = name+" of agility";agi = 120;break;
        case 4: name = name+" of might";str = 65; sta = 65;break;
        case 5: name = name+" of slaying";str = 65; agi = 65;break;
        case 6: name = name+" of tenacity";agi = 65; sta = 65;break;
        case 7: name = name+" of the warlord";str = 45; agi = 45; sta = 45;break;
        }
        Item *i = new Item(name,"swing Dragonbane in a deadly arc, slicing", 500, str, sta, agi,false,type,10000000*cost, true);
        Printer::printer("You have successfully purchased ", i->getColouredName());
        p->items.push_back(i);
    }

    void Dm::megaBattle()
    {
        std::cout << "How many enemies would you like to fight?" << std::endl;
        int number = InputReader::getInt();
        int v;
        do{
            std::cout << "how varied to you want your foes? 0-35" << std::endl;
            v = InputReader::getInt();
        }while(v<0||v>35);
        int d;
        do{
            std::cout << "what difficulty foes do you want 0-" << (35-v) << std::endl;
            d = InputReader::getInt();
        }while(d<0||d>35-v);
        std::vector<Creep*> creeps;
        for(int i=0; i<number;i++)
            creeps.push_back(p->getMegaCreep((int)(Helper::rd()*(v)+d)));
        result(fight(creeps),creeps);

    }

    void Dm::pvp()
    {
        if(!p->getCreep(30)->isEncountered())
        {
            std::vector<Creep*> creeps;
            creeps.push_back(p->getCreep(30));
            fight(creeps);
        }
        std::cout << std::endl;
        std::cout << std::endl;
        std::cout << "Enter the name of the character you wish to duel, fight higher level characters for more honour.(the characters profile must be available to this program eg in the same file)" << std::endl;
        std::cout << "Enter anything else to exit\n" << std::endl;
        std::vector<std::string> names = getCharacters();
        Printer::printCharacters(names);
        std::string target = InputReader::getString();
        if(Helper::contains(names,target))
            pvpFight(target);
        else
            std::cout<<"There is no character called "<<target<<std::endl;

    }

    void Dm::getRandomItem(int v)
    {
        Item * item = new Item(v);
        std::cout << std::endl;
        std::cout << "for your valiant efforts you receive" << std::endl;
        std::cout << std::endl;
        std::cout << std::endl;
        Printer::printer("		", item->getColouredName(),"\n");
        std::cout << "" << std::endl;
        p->items.push_back(item);
    }

    void Dm::getAnotherRandomItem(int v)
    {
        Item *item = new Item(v);
        Printer::printer("		", item->getColouredName(),"\n");
        std::cout << std::endl;
        p->items.push_back(item);
    }

    void Dm::pvpFight(std::string foe)
    {
        Player *f = new Player(foe);
        f->totalStats();
        if(f->agility==10&&f->stamina==10&&f->strength==10)
        {
            std::cout << "That character could not be  found or was unsuitable for a duel" << std::endl;
            return;
        }
        bool fight = true;
        bool pTouched = false;
        bool fTouched = false;
        bool potionsEnabled = false;
        double fAgility =f->agility;
        double fTurn = (1+Helper::rd())/fAgility;
        Creep fC = *f->getCreep(f->companion);
        double fCTurn = (1+Helper::rd())/(double)fC.getAgility();
        double pAgility =p->agility;
        double pTurn = (1+Helper::rd())/pAgility;
        Creep pC = companion;
        double pCTurn = (1+Helper::rd())/(double)pC.getAgility();
        std::string fCName = f->companionName + " " + fC.getName();
        std::string pCName = p->companionName + " " + pC.getName();
        std::cout << "You are level " << p->level << " " << f->name << " is level " << f->level << std::endl;
        std::cout << "1 - to play with potions enabled, 2 - to play with potions disabled" << std::endl;
        if(InputReader::getInt()==1)
            potionsEnabled = true;

        while(p->health>0&&f->health>0&&fight)
        {
            if(fCTurn<=pTurn&&fCTurn<=fTurn&&fTurn<=fTurn)
            {
                int hit;
                hit = fC.getDamage() + (int)(fC.getDamage()*(Helper::rd()+0.1));
                if(!f->achievements.isEarned("Powerful Friends"))
                    hit -= p->gearArmour;
                double dodgeChance = 1-1/(1+(pAgility/((double)fC.getAgility()*4)));
                std::cout << std::endl;
                if(p->achievements.isEarned("Ninja IV")&&Helper::rd()+dodgeChance>=1)
                    std::cout << "You dodge " << fCName << "s attack" << std::endl;
                else if(hit>0)
                {
                    std::cout << fCName << " attacks you for ";
                    Printer::printColouredText(hit,ScrUtil::Red);
                    std::cout << " damage" << std::endl;
                    pTouched = true;
                    p->health -= hit;
                }
                else
                {
                    std::cout << fCName << "s attack  bounces off your armour" << std::endl;
                    pTouched = true;
                }
                std::cout << std::endl;
                fCTurn += (1+Helper::rd())/(double)fC.getAgility();
            }
            else if(pCTurn<=pTurn&&pCTurn<=fCTurn&&pCTurn<=fTurn)
            {
                int hit;
                hit = pC.getDamage() + (int)(pC.getDamage()*(Helper::rd()+0.1));
                if(!p->achievements.isEarned("Powerful Friends"))
                    hit -= f->gearArmour;

                double dodgeChance = 1-1/(1+(fAgility/((double)pC.getAgility()*5)));
                std::cout << std::endl;
                if(f->achievements.isEarned("Ninja IV")&&Helper::rd()+dodgeChance>=1)
                    std::cout << f->name << " dodges " << pCName << "s attack" << std::endl;
                else if(hit>0)
                {
                    std::cout << pCName << " attacks " << f->name << " for ";
                    Printer::printColouredText(hit,ScrUtil::Red);
                    std::cout << " damage" << std::endl;
                    fTouched = true;
                    f->health -= hit;
                }
                else
                    std::cout << pCName << "s attack  bounces off " << f->name << "s armour" << std::endl;
                pCTurn += (1+Helper::rd())/(double)pC.getAgility();
            }
            else if(fTurn<=fCTurn&&fTurn<=pTurn&&fTurn<=pCTurn)
            {
                if(potionsEnabled&&p->damage*(2+p->criticalChance())>=f->health&&p->damage*(2+p->criticalChance())<=f->getMaxHealth()&&p->agility*p->damage*(2+p->criticalChance())<=f->agility*f->potionHeals()&&f->potions>0)
                {
                    drinkPotion(f);
                    fTurn += (1+Helper::rd())/fAgility;
                }
                else if(f->achievements.isEarned("Tank III")&&((f->health>0.2*f->getMaxHealth()&&f->getMaxHealth()>f->damage*(1.5+f->criticalChance()-p->gearArmour))||(f->getMaxHealth()/2>f->damage*(2+f->criticalChance()*0.5)&&!fTouched&&f->achievements.isEarned("Ninja III"))))
                {
                    int hit = f->getMaxHealth()/2;
                    std::cout << f->name << " sacrifices ";
                    Printer::printColouredText((int)(0.2*hit),ScrUtil::Yellow);
                    std::cout << " health to deal ";
                    Printer::printColouredText((int)(f->getMaxHealth()/2),ScrUtil::Red);
                    std::cout << " to you" << std::endl;
                    f->health -= (int)(0.2*hit);
                    p->health -= hit;
                    pTouched = true;
                    vampiricStrike(f->getMaxHealth()/2,f);
                    if(f->achievements.isEarned("Slayer III")&&hit>(0.5*p->getMaxHealth()/2))
                    {
                        std::cout << f->name << " instantly kills you" << std::endl;
                        p->health = 0;
                    }
                    regenerate(f);
                    fTurn += (1+Helper::rd())/fAgility;
                }
                else
                {
                    std::cout << std::endl;
                    int hit = 0;
                    if(!fTouched&&f->achievements.isEarned("Ninja III"))
                        while(hit<2*f->damage)
                            hit = f->damage + ((int)((f->damage)*(Helper::rd()+f->critical())));
                    else
                        hit = f->damage + ((int)((f->damage)*(Helper::rd()+f->critical())))- p->gearArmour;

                    double dodgeChance = 1-1/(1+(pAgility/((double)fAgility*4)));
                    std::cout <<  std::endl;
                    if(p->achievements.isEarned("Ninja IV")&&Helper::rd()+dodgeChance>=1)
                        std::cout << "You dodge " << f->name << "'s attack" << std::endl;
                    else if(hit<=0)
                    {
                        std::cout << f->name << "'s attack bounces pitifully off your armour" << std::endl;
                        pTouched = true;
                    }
                    else
                    {
                        p->health -= hit;
                        if(hit >= 2*f->damage-p->gearArmour){
                            std::cout << f->name << " attacks you for ";
                            Printer::printColouredText(hit,ScrUtil::Red);

                            std::cout << " damage!!" << std::endl;
                        }
                        else
                            Printer::printer(f->name , " attacks you for `r" , hit, "` damage\n");
                        vampiricStrike(hit,f);
                        if(f->achievements.isEarned("Slayer III")&&hit>(0.5*p->getMaxHealth()))
                        {
                            std::cout << f->name << " instantly kills you" << std::endl;
                            p->health = 0;
                        }
                    }
                    regenerate(f);
                    fTurn += (1+Helper::rd())/fAgility;
                }
            }
            else if(pTurn<=fCTurn&&pTurn<=fTurn&&pTurn<=fCTurn)
            {
                std::string creep = f->name;
                Printer::printer("You have `g" , p->health , "` health and " , creep , " has `g" , f->health , "` health.\n");
                std::cout<< "0 - to wait\n 1 - to continue to fight\n 2 - to run away\n";
                if(potionsEnabled)
                     std::cout << " 3 - to use a potion, you have " << p->potions << " potion" << Helper::plural(p->potions) << " left.\n";
                if(p->achievements.isEarned("Tank III"))
                    std::cout << " 4 - to use Sacrificial Strike\n";
                int r =  InputReader::getInt();
                if (r==0)
                {
                    regenerate(p);
                    pTurn += (1+Helper::rd())/pAgility;
                }
                else if(r==1)
                {
                    std::cout << std::endl;
                    int hit = 0;
                    if(!pTouched&&p->achievements.isEarned("Ninja III"))
                        while(hit<2*p->damage)
                            hit = p->damage + ((int)((p->damage)*(Helper::rd()+p->critical())));
                    else
                        hit = p->damage + ((int)((p->damage)*(Helper::rd()+p->critical())))- f->gearArmour;
                    if(hit>100)
                        p->earn("Sucker Punch");
                    if(hit>1000)
                        p->earn("Big Hitter");
                    double dodgeChance = 1-1/(1+(fAgility/((double)pAgility*4)));
                    std::cout << std::endl;
                    if(f->achievements.isEarned("Ninja IV")&&Helper::rd()+dodgeChance>=1)
                        std::cout << f->name << " dodges your attack" << std::endl;
                    if(hit<=0)
                        std::cout << "Your attack bounces pitifully off " << creep << "'s armour" << std::endl;
                    else
                    {
                        f->health -= hit;
                        if(hit >= 2*p->damage-f->gearArmour)
                            Printer::printer("You " , p->attack , " " , creep , " for `y" , hit , "` damage!!\n");
                        else
                            Printer::printer("You " , p->attack , " " , creep , " for `y" , hit , "` damage\n");
                        vampiricStrike(hit,p);
                        if(p->achievements.isEarned("Slayer III")&&hit>(0.5*f->getMaxHealth()))
                        {
                            std::cout << "You  instantly kill " << creep << std::endl;
                            f->health = 0;
                        }
                    }
                    regenerate(p);
                    pTurn += (1+Helper::rd())/pAgility;
                }
                else if(r==2)
                {
                    std::cout << "" << std::endl;
                    std::cout << "You run away like a little girl" << std::endl;
                    std::cout << "" << std::endl;
                    break;
                }
                else if(r==3&&potionsEnabled)
                {
                    if(p->potions>0)
                    {
                        drinkPotion(p);
                        pTurn += (1+Helper::rd())/pAgility;
                        regenerate(p);
                    }
                    else
                        std::cout << "You haven't got any potions" << std::endl;
                }
                else if(r==4&&p->achievements.isEarned("Tank III"))
                {
                    int hit = p->getMaxHealth()/2;
                    Printer::printer("You sacrifice `r" , (int)(0.1*hit) , "` health to deal `y" , p->getMaxHealth()/2 , "` to " , creep,"\n");
                    p->health -= (int)(0.1*hit);
                    f->health -= hit;
                    vampiricStrike(p->getMaxHealth()/2,p);
                    if(p->achievements.isEarned("Slayer III")&&hit>(0.5*f->getMaxHealth()/2))
                    {
                        std::cout << "You  instantly kill " << creep << std::endl;
                        f->health = 0;
                    }
                    regenerate(p);
                    pTurn += (1+Helper::rd())/pAgility;
                }
            }

        }
        if(f->health<=0)
        {
            p->earn("FC");
            if(f->level>=p->level)
                p->honour = p->honour + f->level - p->level;
            if(f->level>=p->level+20)
                p->earn("David");
            std::cout << "Congratulations you defeated  " << f->name << " in single combat" << std::endl;
        }
        else if(p->health<=0)
        {
            std::cout << "unfortunately " << f->name << " defeated you in single combat" << std::endl;
            p->honour -= 3;
        }
        else
        {
            std::cout << "embarrassing" << std::endl;
            p->honour -= 2;
        }
    }

    /**
     * runs a fight and returns 1 if the player won 2 if they lost and 3 if the fled
     * @param creep
     * @return result
     */
    int Dm::fight(std::vector<Creep*> creeps)
    {
        std::vector<int> clife;
        std::vector<double> cturns;
        bool fight =  true;
        bool touched = false;
        double agility = p->agility;
        int damage = p->damage;
        double pturn = (1+Helper::rd())/agility;
        unsigned int target=0;
        int tame = 0;
        std::string companionName = p->companionName + " " + companion.getName();
        double companionTurn = 1/(double)companion.getAgility();
        bool helpedCompanion = false;
        std::cout << "You get in a fight with: " << std::endl;
        for(unsigned int i=0;i<creeps.size();i++)
        {
            std::cout << creeps.at(i)->getName() << std::endl;
            clife.push_back(creeps.at(i)->getLife());
            cturns.push_back((1+Helper::rd())/(double)(creeps.at(i)->getAgility()));
            creeps.at(i)->hasBeenEncountered();
        }
        while(creepsAlive(clife)>0&&p->health>0&&fight&&tame<10)
        {//the fight mechanics
            int r = 0;
            fight = true;
            if(companionTurn<pturn&&Helper::smallerThanAllAlive(companionTurn, cturns, clife)&&!p->passiveCompanion)
            {
                if(clife.at(target)<=0)
                    for(unsigned int i=0;i<clife.size();i++)
                        if(clife.at(i)>0)
                        {
                            target = i;
                            break;
                        }

                int hit;
                hit = companion.getDamage() + (int)(companion.getDamage()*(Helper::rd()+0.1));
                if(!p->achievements.isEarned("Powerful Friends"))
                    hit -= creeps.at(target)->getDr();
                if(hit>0)
                {
                    Printer::printer(companionName," attacks ", creeps.at(target)->getName(), " for `c" , hit ,"` damage\n");
                    clife.at(target) -= hit;
                    if (clife.at(target)<=0)
                        clife.at(target)=0;
                }
                else
                    std::cout << companionName << "s attack  bounces off " << creeps.at(target)->getName() << "s " << creeps.at(target)->getArmour() << std::endl;
                companionTurn += (1+Helper::rd())/(double)companion.getAgility();
            }
            else if(Helper::smallerThanAllAlive(pturn, cturns,clife)&&(pturn<=companionTurn||p->passiveCompanion))
            {
                std::cout << std::endl;
                if(creepsAlive(clife)==1)
                    for(unsigned int i=0;i<creeps.size();i++)
                    {
                        if(clife.at(i)>0)
                            target = i;
                    }
                else
                {

                    do
                    {	Printer::printer("who do you wish to target?, you have `g" , p->health , "` health,  \n\n" , (creeps.size()+1) , " - to run away.\n");
                        for(unsigned int i=0;i<creeps.size();i++)
                        {
                            if(clife.at(i)>0)
                                std::cout << i << ") " << creeps.at(i)->getName() << " " << clife.at(i) << "/" << creeps.at(i)->getLife()  << std::endl;
                        }
                        target = InputReader::getInt();
                        if(target==creeps.size()+1)
                            break;

                    }while(target>=creeps.size()||clife.at(target)<=0);
                }
                if(target==creeps.size()+1)
                {
                    for(unsigned int i=0;i<creeps.size();i++)
                        if(clife.at(i)>0)
                        {
                            target = i;
                            r=2;
                            break;
                        }
                }
                else
                {
                    std::string creep = creeps.at(target)->getName();
                    Printer::printer("You have `g" , p->health , "` health and " , creep , " has `g" , clife.at(target) , "` health.\n");
                    std::cout<< " 0 - to wait\n 1 - to continue to fight\n 2 - to run away\n 3 - to use a potion, you have " << p->potions << " potion" << Helper::plural(p->potions) << " left." << std::endl;
                    if(p->achievements.isEarned("Tank III"))
                        std::cout << " 4 - to use Sacrificial Strike\n" << std::endl;
                    if(!creeps.at(target)->equals("the doppelganger")&&!creeps.at(target)->equals("the GOD")&&!creeps.at(target)->equals("the DM"))
                    {
                        if(tame==0)
                            std::cout << " 8 - to begin befriending " << creep << std::endl;
                        else
                            std::cout << " 8 - to continue befriending " << creep << " you have befriended for " << tame << " turns" << std::endl;
                    }
                    else
                        std::cout << "you cannot befriend " << creep << std::endl;
                    r =  InputReader::getInt();
                }
                std::string creep = creeps.at(target)->getName();
                if (r==0)
                {
                    regenerate(p);
                    pturn += (1+Helper::rd())/agility;
                }
                else if(r==1)
                {
                    std::cout << std::endl;
                    int hit = 0;
                    if(!touched&&p->achievements.isEarned("Ninja III"))
                        while(hit<2*damage)
                            hit = damage + ((int)((damage)*(Helper::rd()+p->critical())));
                    else
                        hit = damage + ((int)((damage)*(Helper::rd()+p->critical())))- creeps.at(target)->getDr();
                    if(hit>100)
                        p->earn("Sucker Punch");
                    if(hit>1000)
                        p->earn("Big Hitter");
                    if(hit>=creeps.at(target)->getLife()&&clife.at(target)>=creeps.at(target)->getLife())
                    {
                        if(creeps.at(target)->equals("the ticking easter egg"))
                            p->earn("Easter");
                        if(creeps.at(target)->equals("the doppelganger"))
                            p->earn("Deadly");
                        p->earn("Make It Count");
                    }

                    if(hit<=0)
                        std::cout << "Your attack bounces pitifully off " << creep << "'s " << creeps.at(target)->getArmour() << std::endl;
                    else
                    {
                        helpedCompanion = true;
                        clife.at(target) -= hit;
                        if(hit >= 2*damage-creeps.at(target)->getDr())
                            Printer::printer("You " , p->attack , " " , creep , " for `y" , hit , "` damage!!\n");
                        else
                            Printer::printer("You " , p->attack , " " , creep , " for `y" , hit , "` damage\n");
                        vampiricStrike(hit,p);
                        if(p->achievements.isEarned("Slayer III")&&hit>(0.5*creeps.at(target)->getLife()))
                        {
                            std::cout << "You  instantly kill " << creep << std::endl;
                            clife.at(target) = 0;
                        }
                        for(unsigned int i=0;i<creeps.size();i++)
                        {
                            if(creeps.at(i)->equals("Buddha")&&clife.at(i)>0)
                            {
                                Printer::printer( "Buddha reflects your negative energy back upon you for `r" , hit , "` damage\n");
                                p->health -= hit;
                            }
                        }

                    }
                    regenerate(p);
                    pturn += (1+Helper::rd())/agility;
                }
                else if(r==2)
                {
                    std::cout << "" << std::endl;
                    std::cout << "" << std::endl;
                    fight = false;
                    if((int)(2*creeps.at(target)->getAgility()/agility)*Helper::rd()>=1)
                    {
                        int hit;
                        hit = creeps.at(target)->getDamage() + (int)(creeps.at(target)->getDamage()*(Helper::rd()+0.1))- p->gearArmour;
                        if(hit>0)
                        {
                            p->health = p->health - hit;
                            std::cout << "You start running away like a little girl" << std::endl;
                            Printer::printer(creep , " " , creeps.at(target)->getAttack() , " you for `r" , hit , "` damage as you try to run away\n\n");
                        }

                    }
                    else
                    {
                        std::cout << "" << std::endl;
                        std::cout << "You run away like a little girl" << std::endl;
                        std::cout << "" << std::endl;
                    }
                    if(p->health>0)
                        p->fledFrom(creeps.at(target));
                }
                else if(r==3)
                {
                    if(p->potions>0)
                    {
                        drinkPotion(p);
                        pturn += (1+Helper::rd())/agility;
                    }
                    else
                        std::cout << "You havn't got any potions" << std::endl;
                }
                else if(r==4&&p->achievements.isEarned("Tank III"))
                {
                    int hit = p->getMaxHealth()/2;
                    Printer::printer("You sacrifice `r" , (int)(0.2*hit) , "` health to deal `y" , p->getMaxHealth()/2 , "` to " , creep ,"\n");
                    p->health -= (int)(0.2*hit);
                    if(hit>=creeps.at(target)->getLife()&&clife.at(target)>=creeps.at(target)->getLife())
                    {
                        if(creeps.at(target)->equals("the ticking easter egg"))
                            p->earn("Easter");
                        if(creeps.at(target)->equals("the doppelganger"))
                            p->earn("Deadly");
                        p->earn("Make It Count");
                    }
                    clife.at(target) -= hit;
                    vampiricStrike(p->getMaxHealth()/2,p);
                    if(p->achievements.isEarned("Slayer III")&&hit>(0.5*creeps.at(target)->getLife()))
                    {
                        std::cout << "You  instantly kill " << creep << std::endl;
                        clife.at(target) = 0;
                    }
                    regenerate(p);
                    pturn += (1+Helper::rd())/agility;

                    for(unsigned int i=0;i<creeps.size();i++)
                    {
                        if(creeps.at(i)->equals("Buddha"))
                        {
                            Printer::printer("Buddha reflects your negative energy back upon you for `r" , hit , "` damage\n" );
                            p->health -= hit;
                        }
                    }
                }
                else if(r==8&&!creeps.at(target)->equals("the doppelganger")&&!creeps.at(target)->equals("the GOD")&&!creeps.at(target)->equals("the DM"))
                {
                    tame++;
                    regenerate(p);
                    pturn += (1+Helper::rd())/agility;
                    companionTurn = pturn;
                }
            }
            else
            {
                int foe = -1;
                for(unsigned int i=0;i<creeps.size();i++)
                {
                    if(Helper::smallerThanAllAlive(cturns.at(i), cturns,clife)&&cturns.at(i)<=pturn&&(cturns.at(i)<=companionTurn||p->passiveCompanion)&&clife.at(i)>0)
                        foe = i;
                }
                if(foe==-1)
                    continue;


                double dodgeChance = 1-1/(1+(agility/((double)creeps.at(target)->getAgility()*4)));
                int hit;
                hit = creeps.at(foe)->getDamage() + (int)(creeps.at(foe)->getDamage()*(Helper::rd()+0.1)) - p->gearArmour;
                if(creeps.at(foe)->equals("the vampire lord")||creeps.at(foe)->equals("Hades"))
                    hit += p->gearArmour;
                if(creeps.at(foe)->equals("Buddha"))
                {
                    std::cout << "Buddha heals himself " << (hit+p->gearArmour) << " health" << std::endl;
                    clife.at(foe) += hit+p->gearArmour;
                    cturns.at(foe) += (1+Helper::rd())/((double)creeps.at(foe)->getAgility());
                    continue;
                }
                if(creeps.at(foe)->equals("The giant flying spaghetti monster"))
                    hit -= creeps.at(foe)->getDamage();
                std::cout << std::endl;
                if(p->achievements.isEarned("Ninja IV")&&Helper::rd()+dodgeChance>=1)
                    std::cout << "You dodge " << creeps.at(foe)->getName() << "'s attack" << std::endl;
                else if(hit<=0)
                {
                    std::cout << creeps.at(foe)->getName() << "'s attack bounces pitifully off your armour" << std::endl;
                    p->earn("Safe");
                    touched = true;
                }
                else
                {
                    if(creeps.at(foe)->equals("the vampire lord")||creeps.at(foe)->equals("Hades"))
                        clife.at(foe) += hit;
                    p->health -= hit;
                    if(hit >= creeps.at(foe)->getDamage()*2 - p->gearArmour)
                        Printer::printer(creeps.at(foe)->getName() , " " , creeps.at(foe)->getAttack() , " for `r" , hit , "` damage!!" );
                    else
                        Printer::printer(creeps.at(foe)->getName() , " " , creeps.at(foe)->getAttack() , " for `r" , hit , "` damage" );
                    if(p->health<=0)
                        p->diedTo(creeps.at(foe));
                    touched = true;
                    if(creeps.at(foe)->equals("the lich")){
                        Creep * minion = p->getCreep(38);
                        creeps.push_back(minion);
                        creeps.push_back(minion);
                        clife.push_back(minion->getLife());
                        clife.push_back(minion->getLife());
                        cturns.push_back(cturns.at(foe)+(1+Helper::rd())/((double)minion->getAgility()));
                        cturns.push_back(cturns.at(foe)+(1+Helper::rd())/((double)minion->getAgility()));
                    }

                }
                cturns.at(foe) += (1+Helper::rd())/((double)creeps.at(foe)->getAgility());
                std::cout << std::endl;
                std::cout << std::endl;
            }
        }

        //win
        if(creepsAlive(clife)==0)
        {
            for(unsigned int i=0;i<clife.size();i++)
                if(creeps.at(i)->equals("the ticking easter egg")&&(cturns.at(i)>2/((double)creeps.at(i)->getAgility())))
                    p->earn("I Got This");
            if(!helpedCompanion)
                p->achievements.earn("Powerful Friends");
                for(unsigned int i=0;i<creeps.size();i++)
                    p->killed(creeps.at(i));
            return 1;
        }
        //die
        else if(p->health<=0)
            return 2;
        else if(tame>=10)
        {
            std::cout << std::endl;
            std::cout << std::endl;
            p->companion = p->getCreepPosition(creeps.at(target));
            std::cout << "You have successfully befriended " << creeps.at(target)->getName() << std::endl;
            std::cout << "What would you like to name your new companion?" << std::endl;
            p->companionName = InputReader::getString();
            std::cout << "Your new companion is " << p->companionName << " " << p->getCreep(p->companion)->getName() << std::endl;

            return 4;
        }
            else
                return 3;
    }

    int Dm::creepsAlive(std::vector<int> clife)
    {
        int alive =0;
        for(unsigned int i=0;i<clife.size();i++)
            if(clife.at(i)>0)
                alive++;
        return alive;
    }

    void Dm::drinkPotion(Player * player)
    {
        int healing = player->potionHeals();
        if(healing + player->health< player->getMaxHealth())
        {
            player->health += healing;
            if(p->equals(player))
                Printer::printer("You pop a potion for `g" , healing , "` health\n" );
            else
                Printer::printer(player->name , " pops a potion for `g" , healing , "` health\n");
        }
        else
        {
            player->health = player->getMaxHealth();
            if(p->equals(player))
                std::cout << "you pop a potion healing yourself to full health" << std::endl;
            else
                std::cout << player->name << " pops a potions healing themself to full health" << std::endl;
        }
        regenerate(player);
        player->potions--;
    }

    void Dm::vampiricStrike(int damage,Player * player)
    {
        int life = player->health;
        if(player->achievements.isEarned("Slayer IV")&&life<player->getMaxHealth())
        {
            if(life+(int)(0.1*damage)>player->getMaxHealth())
            {
                if(p->equals(player))
                    std::cout << "Your vampiric ability heals you full health" << std::endl;
                else
                    std::cout << p->name << "s vampiric ability heals them to full health" << std::endl;
                player->health = player->getMaxHealth();
            }
            else
            {
                if(p->equals(player))
                    Printer::printer("Your vampiric ability heals you `g" , (int)(0.1*damage) , "` health\n" );
                else
                    Printer::printer(p->name , "s vampiric ability heals them `g" , (int)(0.1*damage) , "` health\n");
                player->health += (int)(0.1*damage);
            }
        }
    }

    void Dm::regenerate(Player * player)
    {
        int life = p->health;
        if(player->achievements.isEarned("Tank IV")&&life<player->getMaxHealth())
        {
            if(life+(int)(0.1*player->getMaxHealth())>player->getMaxHealth())
            {
                if(p->equals(player))
                    std::cout << "You regenerate to full health" << std::endl;
                else
                    std::cout << player->name << "regenerates to full health" << std::endl;
                player->health = player->getMaxHealth();
            }
            else
            {
                if(player->equals(p))
                    Printer::printer( "You regenerate `g" , (int)(0.1*player->getMaxHealth()) , "` health\n" );
                else
                    Printer::printer( player->name , "regenerates `g" , (int)(0.1*player->getMaxHealth()) , "` health\n" );
                player->health += (int)(0.1*player->getMaxHealth());
            }
        }
    }

      void Dm::result(int result, std::vector<Creep*> creeps)
    {
        if(result==1)
        {
            std::cout << "" << std::endl;
            std::cout << "" << std::endl;
            std::cout << "Congratulations you defeated: " << std::endl;
            for(unsigned int i=0;i<creeps.size();i++)
                std::cout << creeps.at(i)->getName() << std::endl;
            int totalXp = 0;
            int totalGp = 0;
            for(unsigned int i=0;i<creeps.size();i++)
            {
                totalXp += creeps.at(i)->getXp();
                totalGp += creeps.at(i)->getGp();
            }
            if(creeps.size()<=5)
            {
                totalXp *= creeps.size();
                totalGp *= creeps.size();
            }
            else
            {
                totalXp *= 5;
                totalGp *= 5;
            }
            std::cout << "you gain " << totalXp << "xp and " << totalGp << " gold" << std::endl;
            std::cout << Helper::repeatString(6,"\n");
            p->experience += totalXp;
            p->gold += totalGp;
            bool anyItems = false;
            for(unsigned int i=0;i<creeps.size();i++)
            {
                Creep * c = creeps.at(i);
                double lootChance = c->getLootChance();
                if(p->achievements.isEarned("David"))
                    lootChance += 0.1;
                if(p->achievements.isEarned("Duelist"))
                    lootChance *= 1.1;
                if(p->achievements.isEarned("Pimped"))
                    lootChance *=1.1;
                if(lootChance+Helper::rd()>=1)
                {
                    if(anyItems)
                        getAnotherRandomItem(c->getLootValue());
                    else
                        getRandomItem(c->getLootValue());
                    anyItems = true;
                }
                if(p->questType==1&&p->getCreep(p->questTarget)->equals(c))
                    completeQuest();
            }



        }
        if(result==2)
        {
            std::cout << "" << std::endl;
            std::cout << "" << std::endl;
            p->gold=(int)(p->gold*0.8);
            std::cout << "You have been slain and some of your gold stolen" << std::endl;
            std::cout << "luckily you respawn just outside town" << std::endl;
            std::cout << "" << std::endl;
            std::cout << "" << std::endl;
        }
        //InputReader::pressEnter();
    }

    void Dm::sell(int i)
    {
        double multiplier = 1;
        if(p->achievements.isEarned("Millionaire"))
            multiplier = 1.5;
        else if(p->achievements.isEarned("Just Grand"))
            multiplier = 1.2;
            p->gold += p->items.at(i)->getValue()*multiplier;
            delete(p->items.at(i));
            p->items.erase(p->items.begin()+i);
    }

      void Dm::disenchant(int index)
    {
        std::string cName=p->items.at(index)->getColouredName();
        int power = p->items.at(index)->getStrength()+p->items.at(index)->getStamina()+p->items.at(index)->getAgility();
        delete(p->items.at(index));
        p->items.erase(p->items.begin()+index);
        int shard = 0;
        int number = 1;
        if(power<=0)
            return;
        if(power<=8)
        {
            shard = 0;
            if(power>4)
                number = 2;
        }
        else if(power<=16)
        {
            shard = 1;
            if(power>12)
                number = 2;
        }
        else if(power<=24)
        {
            shard = 2;
            if(power>20)
                number = 2;
        }
        else if(power<=30)
        {
            shard = 3;
            if(power>28)
                number = 2;

        }
        else if(power<=38)
        {
            shard = 4;
            if(power>34)
                number = 2;
        }
        else if(power<=50)
        {
            shard = 5;
            if(power>42)
                number = 2;
            if(power>46)
                number = 3;
        }
        else if(power<100)
        {
            shard = 6;
            if(power>62)
                number = 2;
            else if(power>70)
                number = 3;
            else if(power>78)
                number = 4;
            else if(power>84)
                number = 5;
            else if(power>90)
                number = 6;
        }
        else
            shard = 7;

        for(int i = 0;i<number;i++)
            p->items.push_back(new Item(7,shard));
        Printer::printer("you have succesfully disenchanted ", cName, " into\n");
        std::cout << coreItemName(7,shard) << "x" << number << std::endl;
    }

      void Dm::preUpgradeProcess(Item * item, int tier)
    {

         Printer::printUpgradeOptions(tier, p);

        int c = InputReader::getInt();
        if(c==tier)
            upgradeProcess(item, 100, c-1,tier);
        else if(c>=1&&c<=8)
            upgradeProcess(item, (int)pow(2,6+c-tier), c-1,tier);

    }

    void Dm::upgradeProcess(Item * b, int chance, int shardTier, int tier)
    {
        std::string shard = Item(7,shardTier).getName();
        int numberOfItem = p->numberOfItem(shard,7);
        if(chance==100)
        {
            if(numberOfItem>=1)
            {
                for(unsigned int i = 0; i<p->items.size(); i++)
                    if(p->items.at(i)->equals(shard,7))
                        {
                            p->items.erase(p->items.begin()+i);
                            upgrade(b,tier);
                            std::cout << "upgrade successful" << std::endl;
                            break;
                        }
            }
            else
                std::cout << "you do not have any " << shard << "s" << std::endl;
        }
        else
        {
            std::cout << "how many " << shard << "s do you want to use?" << std::endl;
            int used = InputReader::getInt();

            if(used<=numberOfItem)
            {
                std::cout << "are you sure you wish to expend " << used << " " << shard << "s?" << std::endl;
                std::cout << "the chance of success is " << (chance*used) << "%" << std::endl;
                std::cout << "1 - to use the shards" << std::endl;
                std::cout << "2 - to cancel" << std::endl;
                int attempt = InputReader::getInt();
                if(attempt==1)
                {
                    if((100*Helper::rd())+(used*chance)>=100)
                    {
                        upgrade(b,tier);
                        std::cout << "upgrade successful" << std::endl;
                    }
                    else
                        std::cout << "the upgrade has failed but the item has been salvaged" << std::endl;
                    int item = 0;
                    for(int i = 0; i<used;)
                    {
                        if(p->items.at(item)->equals(shard,7))
                        {
                            p->items.erase(p->items.begin()+item);
                            i++;
                        }
                        else
                            item++;
                    }
                }
            }
            else
                std::cout << "you do not have enough " << shard << "s" << std::endl;
        }
    }

    /**
    *this method upgrades an items stats if given an item object
    */
      void Dm::upgrade(Item * item, int tier)
    {
        if(tier>1)
            item->setName(item->getName().replace(1,1,Helper::intToString(tier)));

        else
            item->setName("+1 "+item->getName());
        item->addBaseStat(1+(int)((1+item->getBaseStat())*0.1));
        item->addStrength(1+(int)((1+item->getStrength())*0.1));
        item->addStamina(1+(int)((1+item->getStamina())*0.1));
        item->addAgility(1+(int)((1+item->getAgility())*0.1));
        item->setValue((int)(item->getValue()*1.2));
        item->augment(true);

        if(p->questType==2&&p->questTarget==tier)
            completeQuest();
    }

      void Dm::completeQuest()
    {
        p->questProgress++;
         Printer::printQuestInfo(p);
        if(p->questProgress<p->questGoal)
            return;
        std::cout << "Quest Complete!" << std::endl;
         Printer::printQuestRewards(p);
        p->questPoints++;
        p->earn("Handyman");
        if(p->questPoints>=10)
            p->earn("Do-gooder");
        if(p->questPoints>=50)
            p->earn("The Seeker");
        if(p->questPoints>=100)
            p->earn("Mayors Best Friend");
        std::cout << "you have completed " << p->questPoints << " quest" << Helper::plural(p->questPoints) << " so far" << std::endl;
        if(p->questType==1)
        {
            p->gold += p->getCreep(p->questTarget)->getGp()*p->questGoal+p->questTarget*10;
            p->experience += p->getCreep(p->questTarget)->getXp()*p->questGoal*2;
        }
        if(p->questType==2)
        {
            for(int i=0;i<2*p->questGoal;i++)
                p->items.push_back(new Item(7,p->questTarget-1));
            p->experience += p->questGoal*p->questTarget*p->questTarget*100;
        }
        resetQuest();
    }

      void Dm::resetQuest()
    {
        p->questType=0;
        p->questTarget=0;
        p->questProgress=0;
        p->questGoal=0;
    }
      void Dm::printItems()
    {
        for(unsigned int i=0; i<p->items.size();i++)
        {
            Item *a = p->items.at(i);
            if(a->getType()!=7)
            {
                std::cout << std::endl;
                Printer::printer(i+1 , ") " , a->getColouredName() , "	Value:" , a->getValue() , "gp	\n" );
                std::cout << "	a:" << a->getBaseStat() << "	Str:" << a->getStrength() << "	Sta:" << a->getStamina() << "	Agi:" << a->getAgility() << std::endl;
            }
        }
    }

    void Dm::incrementTurn()
    {
        p->time++;
        createMarket();
        if(p->questType==-1)
            p->questType = 0;
    }

    void Dm::createMarket()
    {
        if(p->marketItems.size()>0){ //delete p->marketItems.at(0);
            delete p->marketItems.at(0);
        }
        //delete(p->marketItems.at(i));

        p->marketItems.clear();
        for(int i = 0; i<p->level*2&&i<60; i+=3){
            p->marketItems.push_back(new Item(i));
        }
    }


    void Dm::playerCreateItem()
    {
        int l;
        if(p->numberOfItem(QUALITYSHARD,7)==0&&p->numberOfItem(BRILLIANTSHARD,7)==0&&p->numberOfItem(SUPERIORSHARD,7)==0&&p->numberOfItem(EPICSHARD,7)==0&&p->numberOfItem(LEGENDARYSHARD,7)==0)
        {
            std::cout << "you do not have any shards powerful enough to create an item, you need quallty shards or greater" << std::endl;
            return;
        }
        while(true)
        {
             Printer::itemCreationMessages(p,1);
            l = InputReader::getInt();
            if(l==7)
                 Printer::itemCreationMessages(p,2);
            else if(l>0&&l<7)
                break;
            else
                return;
        }
        Printer::itemCreationMessages(p,3);
        std::cout << "you have " << p->gold << "gp" << std::endl;
        int itemStat[4];
        bool created = false;
        std::string shard = "";
        int possiblePoints = 0;
        int pointCost = 0;
        int tier;
        while(true)
        {
            tier = InputReader::getInt();
            switch(tier)
            {
                case 1:
                {
                    shard = QUALITYSHARD;
                    possiblePoints = 25;
                    pointCost = 100;
                    break;
                }
                case 2:
                {
                    shard = BRILLIANTSHARD;
                    possiblePoints = 50;
                    pointCost = 200;
                    break;
                }
                case 3:
                {
                    shard = SUPERIORSHARD;
                    possiblePoints = 80;
                    pointCost = 300;
                    break;
                }
                case 4:
                {
                    shard = EPICSHARD;
                    possiblePoints = 120;
                    pointCost = 400;
                    break;
                }
                case 5:
                {
                    shard = LEGENDARYSHARD;
                    possiblePoints = 200;
                    pointCost = 1000;
                    break;
                }
                case 0:
                    return;
                default:
                    std::cout << "that is not an option" << std::endl;
            }
            if(p->numberOfItem(shard,7)<1&&possiblePoints>0)
                std::cout << "you do not have any " << shard << "s with which to create this item" << std::endl;
            else if(possiblePoints>0)
            {
                std::cout << "you have " << p->gold << "gp" << std::endl;
                break;
            }
        }

        while(true)
        {
            std::cout << "Enter 0-" << possiblePoints << " for how high you want the base stat to be, it cost " << pointCost << "gp for each point" << std::endl;
            int points = InputReader::getInt();
            if(points==911)
                return;
            if(points*pointCost>p->gold||points>possiblePoints||points<0)
                std::cout << "you are not able to do that" << std::endl;
            else
            {
                p->gold -= points*pointCost;
                p->removeItems(shard,7,1);
                itemStat[0] = points;
                created = true;
                break;
            }
        }
        if(created)
        {
            for(int i=0;i<3;i++)
            {
                std::string stat = "";

                if(i==0)
                    stat = "Strength";
                if(i==1)
                    stat = "Stamina";
                if(i==2)
                    stat = "Agility";

                std::cout << "for 0 stat points in " << stat << " enter 0" << std::endl;
                Printer::itemCreationMessages(p,4);
                bool validStats = false;
                while(!validStats)
                {

                    int statChoice = InputReader::getInt();
                    switch(statChoice)
                    {
                        case 0:
                        {
                            itemStat[i+1] = 0;
                            validStats = true;
                            break;
                        }
                        case 1:
                        {
                            possiblePoints = 6;
                            pointCost = 20;
                            shard = QUALITYSHARD;
                            break;
                        }
                        case 2:
                        {
                            possiblePoints = 8;
                            pointCost = 50;
                            shard = BRILLIANTSHARD;
                            break;
                        }
                        case 3:
                        {
                            possiblePoints = 10;
                            pointCost = 100;
                            shard = SUPERIORSHARD;
                            break;
                        }
                        case 4:
                        {
                            possiblePoints = 12;
                            pointCost = 5000;
                            shard = EPICSHARD;
                            break;
                        }
                        case 5:
                        {
                            possiblePoints = 16;
                            pointCost = 20000;
                            shard = LEGENDARYSHARD;
                            break;
                        }
                        case 911:
                            return;
                        default:
                            std::cout << "that is not an option" << std::endl;
                            break;
                    }

                    if(p->numberOfItem(shard,7)<1&&possiblePoints>0&&!validStats)
                        std::cout << "you do not have any " << shard << "s with which to create this item" << std::endl;
                    else if(possiblePoints>0)
                    {
                        std::cout << "you have " << p->gold << "gp" << std::endl;
                        break;
                    }
                }

                while(true)
                {
                    std::cout << "Enter 0-" << possiblePoints << " for how high you want the " << stat << " stat to be, it cost " << pointCost << "gp for each point" << std::endl;
                    int points = InputReader::getInt();
                    if(points==911)
                        return;
                    if(points*pointCost>p->gold||points>possiblePoints||points<0)
                        std::cout << "you are not able to do that" << std::endl;
                    else
                    {
                        p->gold -= points*pointCost;
                        p->removeItems(shard,7,1);
                        itemStat[i+1] = points;
                        break;
                    }
                }
            }

        }
        std::cout << "What is the name of this item?" << std::endl;
        std::string name = InputReader::getString();
        std::string attack = "attack";
        if(l==1)
        {
            std::cout << "attack type eg you [attack type goes here] the enemy" << std::endl;
            attack = InputReader::getString();
        }
        p->items.push_back(new Item(name,attack,itemStat[0],itemStat[1],itemStat[2],itemStat[3], false, l,0,false));
        return;
    }


int Dm::experienceLoss(){
    int loss = (p->level-p->questTarget+5)*p->level*10;
    if (loss<0)
        return 0;
    return loss;
}

std::vector<std::string> Dm::getCharacters(){
    const char* PATH = "saves";

    DIR *dir = opendir(PATH);

    struct dirent *entry = readdir(dir);
    std::vector<std::string> names;

    while (entry != NULL)
    {
        if(entry->d_name[0]!='.')
            names.push_back(entry->d_name);
        entry = readdir(dir);
    }

    closedir(dir);
    return names;
}


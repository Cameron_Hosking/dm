#include "achievement.h"
void Achievement::print(){

        //prints a line of asterisks
        std::cout <<"   " << Helper::repeatString(50,"*") << std::endl;
        //prints a line with the name of the achievement centred
        printLine(name);

        // prints the line with achievement points on it
        //the integer points must first be converted to a String then combined with
        //" Achievement Points" the resulting String is then sent to the printLine method
        printLine(Helper::intToString(points)+" Achievement Points");

        //prints the line with the reason centred
        printLine(reason);

        if(reasonLineTwo.compare("")!=0)
            printLine(reasonLineTwo);

        //another line of asterisks
        std::cout <<"   " << Helper::repeatString(50,"*") << std::endl;

}
    bool Achievement::hasPerk(){
        if(perk.compare("")==0)
            return false;
        return true;
    }

    /**
     * this prints a nice line exactly 48 wide with an asterisk on each side
     */
    void Achievement::printLine(std::string string)
    {
        //the number of spaces to print before the string
        int spaces = (int)((48-string.length())/2);;
        std::cout<<"   *" <<Helper::repeatString(spaces," ") << string << Helper::repeatString(48-(spaces+string.length())," ")<< "*"<<std::endl;
    }
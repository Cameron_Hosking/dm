#include "creep.h"

void Creep::initilize(int clife, int cdamage, int cagility, std::string cattack, std::string carmour, int cdr, int cxp, int cgp, double clootChance, int clootValue)
{
    life = clife;
    damage = cdamage;
    agility = cagility;
    attack = cattack;
    armour = carmour;
    dr = cdr;
    xp = cxp;
    gp = cgp;
    lootChance = clootChance;
    lootValue = clootValue;
}

void Creep::result(int i)
{
    if(i==1)
        kills++;
    else if(i==2)
        deaths++;
    else
        flees++;
}

void Creep::setEncounterStats(bool encountered, int kills, int deaths, int flees)
{
    this->encountered = encountered;
    this->kills = kills;
    this->deaths = deaths;
    this->flees = flees;
}

void Creep::printStats()
{
    if(encountered)
    {
        std::cout << creep << std::endl;
        std::cout << "health: " << life << std::endl;
        std::cout << "Power: " << damage << std::endl;
        std::cout << "speed: " << agility << std::endl;
        if(kills>0)
        {
            std::cout << xp << " xp" << std::endl;
            std::cout << gp << " gp" << std::endl;
        }
        std::cout << kills << " kills" << std::endl;
        std::cout << deaths << " deaths" << std::endl;
        std::cout << flees << " flees" << std::endl;
        std::cout << std::endl;
    }
}

bool Creep::equals(Creep *c)
{
    if(this->creep.compare(c->getName())==0)
        return true;
    return false;
}

bool Creep::equals(std::string name){
    if(creep.compare(name)==0)
        return true;
    return false;
}

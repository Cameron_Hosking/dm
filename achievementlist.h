#ifndef ACHIEVEMENTLIST_H
#define ACHIEVEMENTLIST_H
#include "Helper.h"
#include "achievement.h"


class AchievementList
{
public:
    AchievementList();

    AchievementList(const char* fileName);

     /**
     *saves the achievement data to a file
     */
     void saveAchievements(const char* fileName);
     void printEarned();
     void printUnearned();
     /**
      * @brief earns the achievement if it hasn't been earned already and prints it to the screen
      * @param name of the achievement
      * @return points earned
      */
     int earn(std::string name);
     bool isEarned(std::string name);
     std::vector<Achievement> achievements;

private:
     /**
      * @brief adds all achievements to the achievement vector
      */
     void addAchievements();
};

#endif // ACHIEVEMENTLIST_H

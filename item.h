#ifndef ITEM_H
#define ITEM_H
#include "Helper.h"
#define WEAKSHARD "weak shard"
#define SMALLSHARD "small shard"
#define QUALITYSHARD "quality shard"
#define BRILLIANTSHARD "brilliant shard"
#define SUPERIORSHARD "superior shard"
#define EPICSHARD "epic shard"
#define LEGENDARYSHARD "legendary shard"
#define DRAGONSHARD "dragon shard"
class Item
{


public:
    static Item createShard(int tier);
    Item(int type, int tier);
    Item()
        :wearing(false),augmented(false){}
    Item(std::string nameP, std::string attackP, int baseStatP, int strengthP, int staminaP, int agilityP, bool wearingP, int typeP, int valueP, bool augmentedP)
        :name(nameP), attack(attackP), baseStat(baseStatP), strength(strengthP), stamina(staminaP), agility(agilityP), wearing(wearingP), type(typeP), value(valueP), augmented(augmentedP)
        {}
    Item(int v);
    void setName(std::string name){this->name = name;}
    void setAttack(std::string attack){this->attack = attack;}
    void setBaseStat(int baseStat){this->baseStat = baseStat;}
    void addBaseStat(int baseStatAdded){baseStat += baseStatAdded;}
    void setStrength(int strength){this->strength = strength;}
    void addStrength(int strengthAdded){strength += strengthAdded;}
    void setStamina(int stamina){this->stamina= stamina;}
    void addStamina(int StaminaAdded){stamina += StaminaAdded;}
    void setAgility(int agility){this->agility = agility;}
    void addAgility(int agilityAdded){agility += agilityAdded;}
    void setWearing(bool wearing){this->wearing = wearing;}
    void setType(int type){this->type = type;}
    void setValue(int value){this->value = value;}
    void augment(bool augmented){this->augmented = augmented;}
    bool equals(std::string name,int type);
    int statBeforeEnchantment(int currentStat);


    /**
    *accesser methods
    */
    std::string getName(){return name;}
    std::string getColouredName();
    std::string getAttack(){return attack;}
    int getBaseStat(){return baseStat;}
    int getStrength(){return strength;}
    int getStamina(){return stamina;}
    int getAgility(){return agility;}
    bool getWearing(){return wearing;}
    int getType(){return type;}
    int getValue(){return estimateValue();}
    bool getAugmented(){return augmented;}
    
private:
    std::string getNameColour();
    void setDefaults();
    void randomizeStats(int v,double maxStatValue);
    void nameItem(int tier);
    int estimateValue();
    //the full name of the item eg. legendary skullcap of the warlord
    std::string name;
    //the attack style if the item is a weapon
    std::string attack;
    //base stat (armour or damage)
    int baseStat;
    //strength bonus
    int strength;
    //stamina bonus
    int stamina;
    //agility bonus
    int agility;
    //whether the item is currently being worn
    bool wearing;
    //type of item eg weapon or helmet or body etc
    //1=weapon, 2=body, 3=head, 4=shoulders, 5=hands, 6=legs, 7=shard
    int type;
    //how much the item can be sold for
    int value;
    //has this item already been enchanted
    bool augmented;


};
std::string coreItemName(int type, int tier);

#endif // ITEM_H

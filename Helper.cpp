#include "Helper.h"
namespace Helper{
std::string intToString(int integer){
    std::stringstream ss;
    ss << integer;
    return ss.str();
}

std::string repeatString(int repeats,std::string string){
    std::stringstream ss;
    ss << "";
    for(int i=0;i<repeats;i++)
        ss << string;
    return ss.str();
}

double rd(){
    return (double)rand()/(0.01+(double)RAND_MAX);
}
std::string AnOrA(std::string name){
    if(name.at(0)==*"a"||name.at(0)==*"i"||name.at(0)==*"e"||name.at(0)==*"o"||name.at(0)==*"u")
        return "an";
    return "a";
}
std::string plural(int number){
    if(number!=1)
        return "s";
    return "";
}

bool smallerThanAllAlive(double number, std::vector<double> numbers,std::vector<int> life)
{
    for(unsigned int i=0;i<numbers.size();i++)
        if(number>numbers.at(i)&&life.at(i)>0)
            return false;
    return true;
}
std::string mergeStrings(std::vector<std::string> strings){
    std::stringstream ss;
    for(unsigned int i=0;i<strings.size();i++)
        ss << strings.at(i);
    return ss.str();
}

bool startsWith(std::string searching, std::string searchingFor){
    std::size_t found = searching.find(searchingFor);
    if(found==std::string::npos||found>0)
        return false;
    return true;
}
}

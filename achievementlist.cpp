#include "achievementlist.h"
#include "player.h"
AchievementList::AchievementList()
{
    addAchievements();
}

AchievementList::AchievementList(const char* fileName){
    addAchievements();
    std::ifstream achievementFile(fileName);
    if(!achievementFile.is_open()){
        std::cout << "could not read " << fileName<< std::endl;
        return;
    }
    std::string line;
    std::getline(achievementFile,line);
    std::stringstream ss(line);

    ss >> achievements.at(0).earned;

    for(unsigned int i=1;!ss.fail()&&i<achievements.size();i++)
    {
        ss >> achievements.at(i).earned;
    }
    achievementFile.close();
}


/**
*saves the achievement data to a file
*/
void AchievementList::saveAchievements(const char* fileName)
{
    std::string line = "";
    std::stringstream ss(line);

    for(unsigned int i=0;i<achievements.size();i++)
        ss << achievements.at(i).earned << " ";

    std::ofstream achievementFile;
    achievementFile.open (fileName);
    achievementFile << ss.str();
    achievementFile.close();


}

void AchievementList::addAchievements(){
    achievements.push_back(Achievement(10,"Not A Noob","1 Kill"));
    achievements.push_back(Achievement(10,"Knows How To Press 1","20 Kills"));
    achievements.push_back(Achievement(10,"Button Masher","100 Kills"));
    achievements.push_back(Achievement(20,"Carnage","500 Kills"," damage is increased by 0.1% for every kill up to a maximum of +100%"));
    achievements.push_back(Achievement(20,"Kill Collector","1000 Kills"," your maximum damage bonus is increased to +200%"));
    achievements.push_back(Achievement(40,"God Of War","2000 Kills"," your maximum damage bonus is increased to +300%"));
    achievements.push_back(Achievement(0,"Light Footed","Fleeing 10 times"));
    achievements.push_back(Achievement(0,"Pansy","Fleeing 100 times"));
    achievements.push_back(Achievement(10,"The Value Of Coin","Buy an item from the shop"," unlocks the market"));
    achievements.push_back(Achievement(10,"Money Can Buy Happiness","Purchase snazzy pants"));
    achievements.push_back(Achievement(20,"Deadly Spender","Purchase Death's Scythe"));
    achievements.push_back(Achievement(10,"Double Platinum","20 Gold"));
    achievements.push_back(Achievement(10,"Just Grand","1,000 Gold"," you sell items for 20% more"));
    achievements.push_back(Achievement(10,"Ka-Ching!","10,000 Gold"));
    achievements.push_back(Achievement(20,"What Are You Saving For?","100,000 Gold"));
    achievements.push_back(Achievement(30,"Millionaire","1,000,000 Gold"," you sell items for a further 30% more"));
    achievements.push_back(Achievement(40,"Purposeless Wealth","10,000,000 Gold"));
    achievements.push_back(Achievement(50,"Sir Spamalot","100,000,000 Gold"));
    achievements.push_back(Achievement(10,"Strong","20 Strength"));
    achievements.push_back(Achievement(20,"Very Strong","50 Strength"));
    achievements.push_back(Achievement(30,"Super Strength","100 Strength"));
    achievements.push_back(Achievement(50,"Mr Apollo","200 Strength"));
    achievements.push_back(Achievement(10,"Healthy","20 Stamina"));
    achievements.push_back(Achievement(20,"Hard To Kill","50 Stamina"));
    achievements.push_back(Achievement(30,"Die Hard","100 Stamina"));
    achievements.push_back(Achievement(50,"Tarrasque","200 Stamina"));
    achievements.push_back(Achievement(10,"Quick","20 Agility"));
    achievements.push_back(Achievement(20,"Speedy","50 Agility"));
    achievements.push_back(Achievement(30,"Ninja","100 Agility"));
    achievements.push_back(Achievement(50,"Time Traveller","200 Agility"));
    achievements.push_back(Achievement(20,"Competent","All stats to 20"," you get an extra stat point whenever you level up"));
    achievements.push_back(Achievement(30,"Powerful","All stats to 50"," you get an another extra stat point whenever you level up"));
    achievements.push_back(Achievement(50,"Thats A Big Mountain Lion","All stats to 100"," you get yet another extra stat point whenever you level up"));
    achievements.push_back(Achievement(100,"Godlike","All stats to 200"));
    achievements.push_back(Achievement(10,"Armoured","Donning any armour"));
    achievements.push_back(Achievement(20,"Not A Chink","360 armour","your weapon damage is also contributed as armour"));
    achievements.push_back(Achievement(10,"Tasty","Buying a Potion"));
    achievements.push_back(Achievement(20,"Well Stocked","Owning 10 potions"," potions heal 20% more"));
    achievements.push_back(Achievement(40,"The Regenerator","Owning 100 potions"," potions heal an additional 30% more"));
    achievements.push_back(Achievement(10,"Baby Steps","Level 5"));
    achievements.push_back(Achievement(20,"Getting Into It","Level 20"));
    achievements.push_back(Achievement(30,"Loyal Player","Level 50"));
    achievements.push_back(Achievement(50,"Dedicated","Level 75"));
    achievements.push_back(Achievement(100,"Overpowered","Level 100"));
    achievements.push_back(Achievement(10,"FC","win a duel"));
    achievements.push_back(Achievement(10,"Duelist","Earned 10 honour"," increases loot chance from creatures by 10%"));
    achievements.push_back(Achievement(50,"David","Defeat someone 20 levels higher than you"," increases base loot chance by 10%"));
    achievements.push_back(Achievement(30,"Well Built","Earned 50 honour"));
    achievements.push_back(Achievement(50,"Pimped","Earned 100 honour"," increases loot chance from creatures by a further 10%"));
    achievements.push_back(Achievement(20,"New Champion","Defeated the Arena Champion"," unlocks Island of Legends and Item Creation"));
    achievements.push_back(Achievement(30,"Down To Business","Arena Champion Defeated in 150 turns or less"));
    achievements.push_back(Achievement(50,"Quick Kill","Arena Champion Defeated in 100 turns or less"));
    achievements.push_back(Achievement(100,"Record Kill","Arena Champion Defeated in 60 turns or less"));
    achievements.push_back(Achievement(120,"Invincible","Defeat the arena champion without dying"));
    achievements.push_back(Achievement(120,"Brave","Defeat the arena champion without fleeing"));
    achievements.push_back(Achievement(20,"Untouchable","Defeat the arena champion without a scratch"));
    achievements.push_back(Achievement(200,"Absolute Perfection","(Hard)defeat the arena champion without dying","","without fleeing, and in 60 turns or less"));
    achievements.push_back(Achievement(10,"Make It Count","Kill a creep before it attacks"));
    achievements.push_back(Achievement(10,"Safe","Have an attack bounce totally off you armour"));
    achievements.push_back(Achievement(10,"Sucker Punch","Hit for over 100"));
    achievements.push_back(Achievement(10,"Big Hitter","Hit for over 1000"));
    achievements.push_back(Achievement(50,"Easter","One shot a Ticking Easter Egg"));
    achievements.push_back(Achievement(50,"I Got This","Kill a Ticking Easter Egg after it explodes"));
    achievements.push_back(Achievement(50,"Deadly","One shot a Doppelganger"));
    achievements.push_back(Achievement(20,"Gotta Find Them All!","complete the Bestiary"));
    achievements.push_back(Achievement(10,"Purpose","Get any Achievement"));
    achievements.push_back(Achievement(10,"Triple Figures","Get 100 Achievement Points"," unlocks tier 1 enchantments"));
    achievements.push_back(Achievement(20,"Achiever","Get 200 Achievement Points"," unlocks tier 2 enchantments"));
    achievements.push_back(Achievement(30,"Good Achiever","Get 300 Achievement Points"," unlocks tier 3 enchantments"));
    achievements.push_back(Achievement(40,"Bronze Achiever","Get 500 Achievement Points"," unlocks tier 4 enchantments"));
    achievements.push_back(Achievement(50,"Silver Achiever","Get 750 Achievement Points"," unlocks tier 5 enchantments"));
    achievements.push_back(Achievement(60,"Gold Achiever","Get 1000 Achievement Points"," unlocks tier 6 enchantments"));
    achievements.push_back(Achievement(100,"Platinum Achiever","Get 1500 Achievement Points"," unlocks tier 7 enchantments"));
    achievements.push_back(Achievement(200,"DM","Get 2400 Achievement Points"));
    achievements.push_back(Achievement(100,"The God Slayer","Defeat God in the Epic Arena"));
    achievements.push_back(Achievement(100,"Underdog","(Hard)Defeat God before level 50"));
    achievements.push_back(Achievement(200,"????", "(Hard)Let none stand before you "));
    achievements.push_back(Achievement(10,"Handyman","Complete a quest"));
    achievements.push_back(Achievement(20,"Do-gooder","Complete 10 quests"));
    achievements.push_back(Achievement(50,"The Seeker","Complete 50 quests"));
    achievements.push_back(Achievement(100,"Mayors Best Friend","Complete 100 quests"));
    achievements.push_back(Achievement(100,"Completionist","Earn every achievement"));
    achievements.push_back(Achievement(0,"Slayer I",""," increases damage by 10%"));
    achievements.push_back(Achievement(0,"Slayer II",""," grants 10% of your armour to weapon damage"));
    achievements.push_back(Achievement(0,"Slayer III",""," if your first strike does over 50% of the creatures health it is instantly killed"));
    achievements.push_back(Achievement(0,"Slayer IV",""," grants the Vampiric Strike abilty"));
    achievements.push_back(Achievement(0,"Tank I",""," increases Health by 10%"));
    achievements.push_back(Achievement(0,"Tank II",""," increases potion power by 100%"));
    achievements.push_back(Achievement(0,"Tank III",""," grants the Sacrificial Strike ability"));
    achievements.push_back(Achievement(0,"Tank IV",""," grants regeneration during combat"));
    achievements.push_back(Achievement(0,"Ninja I",""," increases critical chance by 10%"));
    achievements.push_back(Achievement(0,"Ninja II",""," doubles the effect agility has on criticals"));
    achievements.push_back(Achievement(0,"Ninja III",""," attacks you deal before you get hit are auto criticals and ignore armour"));
    achievements.push_back(Achievement(0,"Ninja IV",""," allows you to dodge incoming attacks"));
    achievements.push_back(Achievement(10,"Powerful Friends","Companion wins single handedly"," Companions now ignore armour"));
}

void AchievementList::printEarned(){
    for(unsigned int i=0;i<achievements.size();i++)
        if(achievements.at(i).earned)
            achievements.at(i).print();
}

void AchievementList::printUnearned(){
    for(unsigned int i=0;i<achievements.size();i++)
        if(!achievements.at(i).earned)
            achievements.at(i).print();
}


int AchievementList::earn(std::string name)
{
    for(unsigned int i=0;i<achievements.size();i++)
    {
        Achievement a = achievements.at(i);
        if(!name.compare(a.name))
        {
            if(a.earned)
                return 0;
            else
            {
                a.print();
                std::cout << a.getPerk() << std::endl << std::endl;
                achievements.at(i).earned = true;
                //InputReader::pressEnter();
                return a.points;
            }
        }
    }
    std::cout << "oops i spelled "+name+" wrong in the code using the earn method" << std::endl;
    return 0;
}

bool AchievementList::isEarned(std::string name)
{
   for(unsigned int i=0;i<achievements.size();i++)
   {
       Achievement a = achievements.at(i);
       if(!name.compare(a.name))
       {
           if(a.earned)
               return true;
           else
               return false;
       }
   }
   std::cout << "oops I spelled "+name+" wrong in the code using the isEarned method" << std::endl;
   return false;
}
#-------------------------------------------------
#
# Project created by QtCreator 2013-03-25T20:21:03
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = DMC
Win32{
QMAKE_LFLAGS += -static-libgcc
}
CONFIG   += console
CONFIG   -= app_bundle
CONFIG  += c++11

TEMPLATE = app


SOURCES += main.cpp \
    achievement.cpp \
    Helper.cpp \
    InputReader.cpp \
    achievementlist.cpp \
    player.cpp \
    creep.cpp \
    item.cpp \
    Printer.cpp \
    dm.cpp \
    scrutil.cpp

HEADERS += \
    InputReader.h \
    achievement.h \
    Helper.h \
    achievementlist.h \
    player.h \
    creep.h \
    item.h \
    Printer.h \
    dm.h \
    scrutil.h

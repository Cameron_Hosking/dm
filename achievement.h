/**
 *The class for each acheivement that stores its name, point value and achievement requirements (as a string), note that this can be no longer
 *than 48 characters in length
 */
#ifndef ACHIEVEMENT_H
#define ACHIEVEMENT_H
#include "Helper.h"
class Achievement
{
public:
    Achievement(int achievementPoints, std::string achievementName, std::string reasonP,std::string perk="",std::string reasonLineTwoP = "")
        :name(achievementName), reason(reasonP),reasonLineTwo(reasonLineTwoP),perk(perk), points(achievementPoints), earned(false)
    {}
    void print();
    std::string getPerk(){ return perk;}
    bool hasPerk();
std::string name;
std::string reason;
std::string reasonLineTwo;
std::string perk;
int points;
bool earned;

private:
void printLine(std::string string);

};

#endif // ACHIEVEMENT_H

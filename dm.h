#ifndef DM_H
#define DM_H
#include "Helper.h"
#include "achievement.h"
#include "creep.h"
#include "item.h"
#include "player.h"
#include "Printer.h"
#include "InputReader.h"
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
class Dm
{
    //the players character
public:
    Dm();
    void runGame();

private:
    Player * p;
    Creep companion;
    /**
     * this is the main method from here we initiate the player then
     * we navigate to the mainscreen
     * @param arg
     */

    void mainScreen();
    void help();
    bool mainScreenOptions();
    void menu();
    void inventory();
    /**
     * the achievements method allows the user to look at their earned and unearned achievements
     */
    void achievements();
    /**
     * prints the stats of all the creatures the player has fought
     */
    void bestiary();
    void questInfo();
    void companionInfo();
    /**
     * lets the player choose what part of the town to go to and runs the relevant method
     */
      void town();
      void mayor();
    /**
     * allows the  player to purchase items and potions
     */
    void shop();
    void enchant();
    void market();
    void park();
    void regularForest();
    void deepForest();
    void mountain();
    void desolateShores();
    void islandOfLegends();
    void realmOfTheGods();
    void arena();
    void epicArena();
    void ultimateShowdown();
    void secretArea();
    void dragonShop();
    void megaBattle();
    void pvp();
    void getRandomItem(int v);
    void getAnotherRandomItem(int v);
    void pvpFight(std::string foe);
    /**
     * runs a fight and returns 1 if the player won 2 if they lost and 3 if the fled
     * @param creep
     * @return result
     */
    int fight(std::vector<Creep*> creeps);
    int creepsAlive(std::vector<int> clife);
    void drinkPotion(Player * player);
    void vampiricStrike(int damage,Player * player);
    void regenerate(Player * player);
    void result(int result, std::vector<Creep*> creeps);
    void sell(int i);
    void disenchant(int index);
    void preUpgradeProcess(Item *item, int tier);
    void upgradeProcess(Item  *b, int chance, int shardTier, int tier);


    /**
    *this method upgrades an items stats if given an item object
    */
    void upgrade(Item *item, int tier);
    void completeQuest();
    void resetQuest();
    void printItems();
    void incrementTurn();
    void createMarket();
    void playerCreateItem();
    int experienceLoss();

    std::vector<std::string> getCharacters();

};

#endif // DM_H

#ifndef INPUTREADER_H
#define INPUTREADER_H
#include <iostream>
#include <limits>
#include "scrutil.h"

namespace InputReader{
int getInt();

double getDouble();

std::string getString();
void pressEnter ();
void autoPressEnter();

}
#endif // INPUTREADER_H

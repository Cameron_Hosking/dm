#ifndef SCRUTIL_H_INCLUDED
#define SCRUTIL_H_INCLUDED

namespace ScrUtil {

/** Colors */
enum Color {
    Black, Blue, Red, Magenta, Orange,
    Green, Cyan, Yellow, White,Gold,
    UndefinedColor
};

/**
    Describes a position on the screen
*/
struct Position {
    short int row;
    short int column;
};

struct Attributes {
    Color paper;
    Color ink;
};

/**
    Clears the screen
*/
void clear();

/**
    Indicates the color of the text to write
    @param Ink Color Ink
    @param paper background color
*/
void setColors(Color ink, Color background);

/**
    Indicates the color of the text to write
    @param Color Color ink and paper
    @see Attributes
*/
void setColors(Attributes color);

/**
    Gets the attributes in use
    @return The colors as a structure Attributes
    @see Attributes
*/
Attributes getCurrentAttributes();

/**
    Gets the char at position on the screen
    @param The pos. as a structure Position
    @return The char value
    @see Attributes
*/
int getCharacterAt(Position pos);

/**
    Move the cursor to a pos. determined
    @param row The row where to place cursor
    @param column The column where to place cursor
*/
void moveCursorTo(unsigned short int row, int unsigned short column);

/**
    Move the cursor to a pos. determined
    @param Structure containing pos Position pos.
    @see Position
*/
void moveCursorTo(Position pos);

/**
    Returns the max row and column size.
    @return The info. as structure Position
    @see Position
    @note Unix always returns 25x80
*/
Position getConsoleSize();

/**
    Returns the num. Row
    @return The max. num. Row
    @see Position
    @note If the functionality is not supported,
          returns -1 in both fields Position
*/
short int getMaxRows();

/**
    Returns the num. Column
    @return The max. num. Column
    @see getConsoleSize
*/
short int getMaxColumns();

/**
    Returns the pos. cursor
    @return The pos. cursor
    @see getConsoleSize
    @note Unix always returns -1, -1
*/
Position getCursorPosition();

/**
    Hides or displays the cursor
    @param see If true, displays it, if false it hides.
*/
void showCursor(bool see);

}

#endif // SCRUTIL_H_INCLUDED

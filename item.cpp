#include "item.h"

Item::Item(int type, int tier){
    setDefaults();
    this->type = type;
    nameItem(tier);
}
void Item::setDefaults(){
    baseStat = 0;
    strength = 0;
    stamina = 0;
    agility = 0;
    value = 0;
    type = 1;
    attack = "";
    wearing = false;
    augmented = false;
    name = "";
}

Item::Item(int v)
{
    setDefaults();
    int tier;
    double i = Helper::rd();
    if(i<0.1)
    {
        type = 7;
        tier = (int)(v*Helper::rd()/9);
        if(tier==0)
            name = WEAKSHARD;
        else if(tier==1)
            name = SMALLSHARD;
        else if(tier==2)
            name = QUALITYSHARD;
        else if(tier==3)
            name = BRILLIANTSHARD;
        else if(tier==4)
            name = SUPERIORSHARD;
        else if(tier==5)
            name = EPICSHARD;
        else if(tier==6)
            name = LEGENDARYSHARD;
    }
    else
    {
        if(i<=0.25)
            type = 1;
        else if(i<=0.4)
            type = 2;
        else if(i<=0.55)
            type = 3;
        else if(i<=0.7)
            type = 4;
        else if(i<=0.85)
            type = 5;
        else
            type = 6;

        if(v<10)
        {
            tier = 1;
            randomizeStats(v,2);
        }
        else if(v<20)
        {
            tier = 2;
            randomizeStats(v,4);
        }
        else if(v<30)
        {
            tier = 3;
            randomizeStats(v,6);
        }
        else if(v<40)
        {
            tier = 4;
            randomizeStats(v,8);
        }
        else if(v<50)
        {
            tier = 5;
            randomizeStats(v,10);
        }
        else
        {
            tier = 6;
            randomizeStats(v,12);
        }
        nameItem(tier);
        if(type==1)
        {
            if(tier==1)
                baseStat=4;
            else if(tier==2)
                baseStat=8;
            else if(tier==3)
                baseStat=12;
            else if(tier==4)
                baseStat=20;
            else if(tier==5)
                baseStat=50;
            else if(tier==6)
                baseStat=100;
        }
        else if(type==2||type==6)
        {
            if(tier==1)
                baseStat=5;
            else if(tier==2)
                baseStat=10;
            else if(tier==3)
                baseStat=20;
            else if(tier==4)
                baseStat=50;
            else if(tier==5)
                baseStat=80;
            else if(tier==6)
                baseStat=120;
        }
        else if(type==3||type==4||type==5)
        {
            if(tier==1)
                baseStat=1;
            else if(tier==2)
                baseStat=2;
            else if(tier==3)
                baseStat=4;
            else if(tier==4)
                baseStat=10;
            else if(tier==5)
                baseStat=20;
            else if(tier==6)
                baseStat=40;
        }
    }
    value = estimateValue();
}
void Item::randomizeStats(int v,double maxStatValue)
{
    double n = 10/maxStatValue;
    int c = (v+1-10*((int)(v/10)));
    double m = Helper::rd()*c;
    if(m>3)
    {
        strength++;
        for(double i = n; i <= m;i+=n)
            strength++;
    }
    m = Helper::rd()*c;
    if(m>3)
    {
        stamina++;
        for(double i = n; i <= m;i+=n)
            stamina++;
    }
    m =  Helper::rd()*c;
    if(m>3)
    {
        agility++;
        for(double i = n; i <= m;i+=n)
            agility++;
    }
}



std::string coreItemName(int type, int tier){
    std::string coreItem = "";
    if(type==1)
    {
        if(tier==1)coreItem = "shortsword ";
        else if(tier==2)coreItem = "longsword ";
        else if(tier==3)coreItem = "waraxe ";
        else if(tier==4)coreItem = "claymore ";
        else if(tier==5)coreItem = "executioner's axe ";
        else if(tier==6)coreItem = "Deaths Scythe ";
    }
    else if(type==2)
    {
        if(tier==1)coreItem = "leather armour ";
        else if(tier==2)coreItem = "chainmail ";
        else if(tier==3)coreItem = "breastplate ";
        else if(tier==4)coreItem = "scalemail armour ";
        else if(tier==5)coreItem = "full plate ";
        else if(tier==6)coreItem = "God's Suit ";
    }
    else if(type==3)
    {
        if(tier==1)coreItem = "skullcap ";
        else if(tier==2)coreItem = "leather hood ";
        else if(tier==3)coreItem = "steel hat ";
        else if(tier==4)coreItem = "fullhelm ";
        else if(tier==5)coreItem = "glowing hat ";
        else if(tier==6)coreItem = "God's fedora ";
    }
    else if(type==4)
    {
        if(tier==1)coreItem = "cloth shoulderpads ";
        else if(tier==2)coreItem = "leather shoulderpads ";
        else if(tier==3)coreItem = "steel pauldrons ";
        else if(tier==4)coreItem = "oversized pauldrons ";
        else if(tier==5)coreItem = "mithril shoulders ";
        else if(tier==6)coreItem = "magical shoulders ";
    }
    else if(type==5)
    {
        if(tier==1)coreItem = "silk gloves ";
        else if(tier==2)coreItem = "leather gloves ";
        else if(tier==3)coreItem = "chain gloves ";
        else if(tier==4)coreItem = "steel gauntlets ";
        else if(tier==5)coreItem = "spiked gauntlets ";
        else if(tier==6)coreItem = "magic hands ";
    }
    else if(type==6)
    {
        if(tier==1)coreItem = "snazzy pants ";
        else if(tier==2)coreItem = "leather chaps ";
        else if(tier==3)coreItem = "chain leggings ";
        else if(tier==4)coreItem = "steel platelegs ";
        else if(tier==5)coreItem = "mithril platelegs ";
        else if(tier==6)coreItem = "God's Suit pants ";
    }
    else if(type==7)
    {
        if(tier==0)coreItem = WEAKSHARD;
        else if(tier==1)coreItem = SMALLSHARD;
        else if(tier==2)coreItem = QUALITYSHARD;
        else if(tier==3)coreItem = BRILLIANTSHARD;
        else if(tier==4)coreItem = SUPERIORSHARD;
        else if(tier==5)coreItem = EPICSHARD;
        else if(tier==6)coreItem = LEGENDARYSHARD;
        else if(tier==7)coreItem = DRAGONSHARD;
    }
        return coreItem;
}

void Item::nameItem(int tier)
{
    std::string coreItem = coreItemName(type,tier);
    std::string preItem = "";
    std::string postItem = "";
    int totalStats = strength+stamina+agility;
    if(totalStats==0)
        preItem = "";
    else if(totalStats<=6)
        preItem = "simple ";
    else if(totalStats<=12)
        preItem = "quality ";
    else if(totalStats<=18)
        preItem = "brilliant ";
    else if(totalStats<=24)
        preItem = "superior ";
    else if(totalStats<=30)
        preItem = "Epic ";
    else if(totalStats>30)
        preItem = "Legendary ";

    if(strength>0&&stamina==0&&agility==0)
        postItem = "of strength";
    else if(strength==0&&stamina>0&&agility==0)
        postItem = "of stamina";
    else if(strength==0&&stamina==0&&agility>0)
        postItem = "of agility";
    else if(strength>0&&stamina>0&&agility==0)
        postItem = "of might";
    else if(strength>0&&stamina==0&&agility>0)
        postItem = "of slaying";
    else if(strength==0&&stamina>0&&agility>0)
        postItem = "of tenacity";
    else if(strength>0&&stamina>0&&agility>0)
        postItem = "of the warlord";
    name = preItem + coreItem + postItem;

    if(type==1)
    {
        if(tier==1)attack = "jab with the shortsword";
        else if(tier==2)attack = "swing the longsword at";
        else if(tier==3)attack = "hack";
        else if(tier==4)attack = "cleave";
        else if(tier==5)attack = "attempt to execute";
        else if(tier==6)attack = "harvest the soul of";
    }

}

int Item::estimateValue()
{
    value =baseStat*(5+strength+stamina+agility)+baseStat*baseStat+pow(strength+stamina+agility,3);
    if(type==7){
        int tier = 0;
        if(!name.compare(WEAKSHARD))tier=0;
        else if(!name.compare(SMALLSHARD))tier=1;
        else if(!name.compare(QUALITYSHARD))tier=2;
        else if(!name.compare(BRILLIANTSHARD))tier=3;
        else if(!name.compare(SUPERIORSHARD))tier=4;
        else if(!name.compare(EPICSHARD))tier=5;
        else if(!name.compare(LEGENDARYSHARD))tier=6;
        else if(!name.compare(DRAGONSHARD))tier=7;
        value = 10*(int)(pow(tier+2,5)*0.1);
    }
    return value;
}

bool Item::equals(std::string name,int type){
    if(this->name.compare(name)==0&&this->type==type)
        return true;
    return false;
}

std::string Item::getNameColour()
{
    if(type==7){
        return "w";
    }
    int totalStats = statBeforeEnchantment(strength)+statBeforeEnchantment(stamina)+statBeforeEnchantment(agility);
    if(totalStats<=0)
        return "w";
    if(totalStats<=6)
        return "g";
    if(totalStats<=12)
        return "g";
    if(totalStats<=18)
        return "c";
    if(totalStats<=24)
        return "c";
    if(totalStats<=30)
        return "b";
    if(totalStats<=36)
        return "m";
    if(totalStats<=50)
        return "y";
    else
        return "r";
}

int Item::statBeforeEnchantment(int currentStat)
{
    if(name.at(0)!='+')
        return currentStat;
    int enchantedTier = atoi(name.substr(1,1).c_str());
    for(int i=0;i<enchantedTier;++i){
        currentStat -= (currentStat+9)/10;
    }
    return currentStat;
}

std::string Item::getColouredName()
{
    return std::string("`"+getNameColour()+name+"`");
}

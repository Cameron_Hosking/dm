#ifndef HELPER_H
#define HELPER_H
#include <sstream>
#include <iostream>
#include <limits>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <math.h>
#include "scrutil.h"
namespace Helper{
std::string intToString(int integer);
std::string repeatString(int repeats,std::string string);
double rd();
std::string AnOrA(std::string name);
std::string plural(int number);
/**
 * this in fact returns true if the given value is smaller then or equal too all of the values in the array
 * @param number
 * @param array
 * @return smaller or equal to all values
 */
bool smallerThanAllAlive(double number, std::vector<double> numbers,std::vector<int> life);
std::string mergeStrings(std::vector<std::string> strings);
bool startsWith(std::string searching, std::string searchingFor);

template <typename T>
bool contains(std::vector<T> items,T item){
    for(unsigned int i = 0;i<items.size();++i){
        if(item==items.at(i))
            return true;
    }
    return false;
}

}



#endif // HELPER_H

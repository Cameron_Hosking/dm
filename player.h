#ifndef PLAYER_H
#define PLAYER_H
#include "Helper.h"
#include "achievementlist.h"
#include "Creep.h"
#include "item.h"
#include "InputReader.h"

class Player
{
public:
    Player();
    std::string name;
        int level;
        int health;
        //the characters total strength, stamina and agility stats
        int strength;
        int stamina;
        int agility;
        int damage;
        double damageMultiplier;
        //the characters core strength, stamina and agility stats
        int coreStrength;
        int coreStamina;
        int coreAgility;
        //the stats from gear
        int gearStrength;
        int gearStamina;
        int gearAgility;
        int gearArmour;
        int gearAttack;

        //the experience of the character
        int experience;
        int experienceRequired;

       //the attack style of the character
        std::string attack;
        //the items the character is wearing
        Item * weapon;
        Item * body;
        Item * head;
        Item * shoulders;
        Item * hands;
        Item * legs;

        //the number of total kills deaths and flees for the character
        int kills;
        int deaths;
        int flees;
        //how many potions the character has
        int potions;
        int honour;
        int time;
        int gold;
        int achievementPoints;

        int questType;
        int questTarget;
        int questProgress;
        int questGoal;
        int questPoints;

        int companion;
        std::string companionName;
        bool passiveCompanion;

        int difficulty;

        //the characters items
        std::vector<Item*> items;

        std::vector<Item*> marketItems;

        //the characters achievements;
        AchievementList achievements;

        //the creeps that the character can interact with
        std::vector<Creep*> creeps;
    Player(std::string nameP);
    void earnAchievements();
    Creep* getCreep(int index);
    Creep* getCreep(std::string name);
    Creep* getMegaCreep(int index);
    void printCreepStats();
    void gearStats();
    void totalStats();
    double getDamageMultiplier();
    int xpRequired();
    int scalingCost();
    double criticalChance();
    int numberOfItem(std::string name,int type);
    void removeItems(std::string name, int type, int number);
    void killed(Creep *c);
    void diedTo(Creep *c);
    void fledFrom(Creep *c);
    int getCreepPosition(Creep *c);
    void save();
    void levelUp();
    void talents();
    int getMaxHealth();
    void orderItems();
    Item * itemInSlot(int type);
    void earn(std::string name);
    void resetCharacterStats();
    double critical();
    int potionHeals();
    bool equals(Player * p);



private:
    void loadItems(const char*,std::vector<Item*> &items);
    void saveItems(const char*,std::vector<Item*> &tems);
    bool loadCharacter(const char*);
    void saveCharacter(const char*);
    void loadCreeps(const char*);
    void saveCreeps(const char*);
    void createNewPlayer();
    void addCreeps();
    void refreshStats();
};

#endif // PLAYER_H

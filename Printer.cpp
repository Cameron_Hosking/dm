#include "Printer.h"
namespace Printer{
void printStartScreen(){
    std::cout << Helper::repeatString(16,"\n");
    std::cout << "DDDDDDDDDDDDD         MMMMMMMM               MMMMMMMM" << std::endl;
    std::cout << "D::::::::::::DDD      M:::::::M             M:::::::M" << std::endl;
    std::cout << "D:::::::::::::::DD    M::::::::M           M::::::::M" << std::endl;
    std::cout << "DDD:::::DDDDD:::::D   M:::::::::M         M:::::::::M" << std::endl;
    std::cout << "  D:::::D    D:::::D  M::::::::::M       M::::::::::M" << std::endl;
    std::cout << "  D:::::D     D:::::D M:::::::::::M     M:::::::::::M" << std::endl;
    std::cout << "  D:::::D     D:::::D M:::::::M::::M   M::::M:::::::M" << std::endl;
    std::cout << "  D:::::D     D:::::D M::::::M M::::M M::::M M::::::M" << std::endl;
    std::cout << "  D:::::D     D:::::D M::::::M  M::::M::::M  M::::::M" << std::endl;
    std::cout << "  D:::::D     D:::::D M::::::M   M:::::::M   M::::::M" << std::endl;
    std::cout << "  D:::::D     D:::::D M::::::M    M:::::M    M::::::M" << std::endl;
    std::cout << "  D:::::D    D:::::D  M::::::M     MMMMM     M::::::M" << std::endl;
    std::cout << "DDD:::::DDDDD:::::D   M::::::M               M::::::M" << std::endl;
    std::cout << "D:::::::::::::::DD    M::::::M               M::::::M" << std::endl;
    std::cout << "D::::::::::::DDD      M::::::M               M::::::M" << std::endl;
    std::cout << "DDDDDDDDDDDDD         MMMMMMMM               MMMMMMMM" << std::endl;
    std::cout << "v4.5.0" << std::endl;
}

void printMainMenu(Player * p)
{
    std::cout << Helper::repeatString(4,"\n");
    std::cout << "Turn " << p->time << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << p->name << std::endl;
    std::cout << std::endl;
    std::cout << "Level: " << p->level << std::endl;
    std::cout << "Health: " << p->health << std::endl;
    std::cout << "Experience: " << p->experience << std::endl;
    if(p->level<100)
        std::cout << "Experience required till next level up: " << p->xpRequired() << std::endl;
    std::cout << "Gold: " << p->gold << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "where do you want to go?" << std::endl;
    std::cout << "1 - the menu, includes inventory, achievements," << std::endl;
    std::cout << "    bestiary and quest and companion info" << std::endl;
    std::cout << "2 - go to the town" << std::endl;
    std::cout << "3 - go to the park" << std::endl;
    std::cout << "4 - go into the forest" << std::endl;
    std::cout << "5 - go into the deep forest" << std::endl;
    std::cout << "6 - go to the mountains" << std::endl;
    if(p->achievements.isEarned("New Champion")){
        std::cout << "7 - go to the Desolate Shores" << std::endl;
        std::cout << "8 - go to the Island Of Legends" << std::endl;
    }
    std::cout << "9 - enter the arena, you must wager " << p->scalingCost() << " gold to enter the arena, (all gold won will be confiscated, you may only keep the final reward)." << std::endl;
    if(p->achievements.isEarned("New Champion")){
        std::cout << "10 - enter the epic arena, you must wager " << 2*p->scalingCost() << " gold to enter," << std::endl;
        std::cout << "    (all gold won will be confiscated, you may only keep the final reward)." << std::endl;
    }
    std::cout << "11 - enter a duel" << std::endl;
    if(p->difficulty==2&&p->getCreep("the GOD")->getKills()>0)
        std::cout << "12 - enter the realm of the gods" << std::endl;
    if(p->difficulty==2&&p->achievements.isEarned("Gotta Find Them All!"))
        std::cout << "13 - enter the ultimate showdown of ultimate destiny" << std::endl;

    std::cout << "0 - exit" << std::endl;
}

void printMenu()
{
    std::cout << std::endl;
    std::cout << "1 - your inventory" << std::endl;
    std::cout << "2 - achievements" << std::endl;
    std::cout << "3 - the bestiary" << std::endl;
    std::cout << "4 - quest information" << std::endl;
    std::cout << "5 - companion information" << std::endl;
    std::cout << "0 - cancel" << std::endl;
}

void achievementsMenu(int points)
{
    std::cout << "	You have "<< points<<" Achievement Points" << std::endl;
    std::cout << std::endl;
    std::cout << "1 - look at your earned achievements" << std::endl;
    std::cout << "2 - look at your unearned achievements" << std::endl;
    std::cout << "3 - look at your perks" << std::endl;
    std::cout << "4 - look at your unearned perks" << std::endl;
    std::cout << "0 - exit" << std::endl;
}

void printEarnedPerks(Player * p)
{
    for(std::vector<Achievement>::iterator it = p->achievements.achievements.begin();it!=p->achievements.achievements.end();++it){
        if(it->earned&&it->hasPerk())
            std::cout << it->name << std::endl << it->perk << std::endl << std::endl;
    }
}

void printUnearnedPerks(Player * p)
{
    for(std::vector<Achievement>::iterator it = p->achievements.achievements.begin();it!=p->achievements.achievements.end();++it){
        if(!it->earned&&it->hasPerk())
            std::cout << it->name <<" - "<< it->reason << std::endl << it->perk << std::endl << std::endl;
    }

}

void townMenu(Player * p)
{
    std::cout << "1 - to enter the Mayors Office" << std::endl;
    std::cout << "2 - to enter the shop" << std::endl;
    std::cout << "3 - to enchant your items" << std::endl;
    if(p->achievements.isEarned("The Value Of Coin"))
        std::cout << "4 - to enter the market" << std::endl;
    if(p->achievements.isEarned("New Champion"))
        std::cout << "5 - to enter the Blacksmith" << std::endl;
    std::cout << "0 - to leave the town" << std::endl;
}

void printQuest(Player * p)
{
    std::cout << Helper::repeatString(4,"\n");
    if(p->questType==1)
    {
        std::cout << "Hello young whippersnapper I want you to deal with a little problem for us." << std::endl;
        std::cout << p->getCreep(p->questTarget)->getName()<<"s are being quite a nuisance to us and so i'd like you to go out and slay " << p->questGoal << " for me, good luck." << std::endl;
    }
    if(p->questType==2)
    {
        std::cout << "Hello young wippersnapper I think this world needs more magic, make it so." << std::endl;
        std::cout << "Create " << p->questGoal << " +" << p->questTarget << " item"<<Helper::plural(p->questGoal)<<" and i will repay your efforts" << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Quest" << std::endl;
    printQuestInfo(p);
    printQuestRewards(p);
}


void printQuestInfo(Player * p)
{
    if(p->questType==1)
        std::cout << "kill " <<  p->getCreep(p->questTarget)->getName()<<Helper::plural(p->questGoal)<<" " << p->questProgress << "/" << p->questGoal << std::endl;
    if(p->questType==2)
        std::cout << "Enchant item"<<Helper::plural(p->questGoal)<<" up to +" << p->questTarget << "  " << p->questProgress << "/" << p->questGoal << std::endl;
}

void printQuestRewards(Player * p)
{
    std::cout << std::endl;
    if(p->questType==1)
    {
        std::cout << "Rewards:" << std::endl;
        std::cout << p->getCreep(p->questTarget)->getGp()*p->questGoal+p->questTarget*10 << "gp  +  " << (p->getCreep(p->questTarget)->getXp()*p->questGoal*2) << " experience" << std::endl;
    }
    if(p->questType==2)
    {
        std::cout << "Rewards:" << std::endl;
        std::cout << coreItemName(7,p->questTarget-1)<<" x"<<2*p->questGoal<<"  +  "<<p->questGoal*p->questTarget*p->questTarget*100<<" experience" << std::endl;
    }
}

/**
 * prints all the items that are not shards in the given items array
 */
void printAllItems(std::vector<Item*> items)
{
    for(unsigned int i=0; i<items.size();i++)
    {
        Item * a = items.at(i);
        if(!a->getWearing()&&a->getType()!=7)
        {
            std::cout << std::endl;
            Printer::printer(i+1,") ",a->getColouredName(),"	Value:",a->getValue(),"gp	\n");
            std::cout << "	a:" << a->getBaseStat()<<"	Str:"<<a->getStrength()<<"	Sta:"<<a->getStamina()<<"	Agi:"<<a->getAgility() << std::endl;
        }
        else if(!a->getWearing()&&a->getName().compare(WEAKSHARD)&&a->getName().compare(SMALLSHARD)&&a->getName().compare(QUALITYSHARD)&&a->getName().compare(BRILLIANTSHARD)&&a->getName().compare(SUPERIORSHARD)&&a->getName().compare(EPICSHARD)&&a->getName().compare(LEGENDARYSHARD)&&a->getName().compare(DRAGONSHARD))
            Printer::printer(i,") ",a->getColouredName(),"	Value:",a->getValue(),"\n");
    }
}

/**
 * prints the characters name, health, strength, stamina, agility,
 * potions, honour, experience, experience until they level, gold, and everything they are wearing
 */
void printInventoryStats(Player * p)
{
    p->totalStats();
    std::cout << Helper::repeatString(6,"\n");
    std::cout << p->name << std::endl;
    std::cout << std::endl;
    std::cout << "Level: " << p->level << std::endl;
    std::cout << "Health: " << p->health << std::endl;
    std::cout << "Strength: " << p->strength << " (" << p->gearStrength << ")" << std::endl;
    std::cout << "Stamina: " << p->stamina << " (" << p->gearStamina << ")" << std::endl;
    std::cout << "Agility: " << p->agility << " (" << p->gearAgility << ")" << std::endl;
    std::cout << "Critical chance : " << ((double)(int)(1000*p->criticalChance()+2/3))/10 << "%" << std::endl;
    std::cout << "Potions: " << p->potions << std::endl;
    std::cout << "honour: " << p->honour << std::endl;
    std::cout << "Experience: " << p->experience << std::endl;
    if(p->level<100)
        std::cout << "Experience required till next level up: " << p->xpRequired() << std::endl;
    std::cout << "Gold: " << p->gold << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Your gear:" << std::endl;
    std::cout << "weapon: " << std::endl;
    printItemStats(p->weapon);
    std::cout << "Body: " << std::endl;
    printItemStats(p->body);
    std::cout << "head: " << std::endl;
    printItemStats(p->head);
    std::cout << "shoulders: " << std::endl;
    printItemStats(p->shoulders);
    std::cout << "gloves: " << std::endl;
    printItemStats(p->hands);
    std::cout << "legs: " << std::endl;
    printItemStats(p->legs);
    std::cout << "Total armour: " << p->gearArmour << std::endl;
    std::cout << std::endl;
}

/**
 * prints the upgrade options given the power of the item being upgraded and an array of
 * items to search for shards
 */
void printUpgradeOptions(int tier, Player * p)
{
    if(tier==1)
        std::cout << "you need 1 weak shard to enchant this item" << std::endl;
    if(tier==2)
        std::cout << "you need 1 small shard to enchant this to +2" << std::endl;
    if(tier==3)
        std::cout << "you need 1 quality shard to enchant this to +3" << std::endl;
    if(tier==4)
        std::cout << "you need 1 brilliant shard to enchant this to +4" << std::endl;
    if(tier==5)
        std::cout << "you need 1 superior shard to enchant this to +5" << std::endl;
    if(tier==6)
        std::cout << "you need 1 epic shard to enchant this to +6" << std::endl;
    if(tier==7)
        std::cout << "you need 1 Legendary shard to enchant this to +7" << std::endl;

    if(tier>6)
        std::cout << "or you can use epic shards with an accumulating " << (int)pow(2,12-tier)  <<  "% chance of success per shard" << std::endl;
    if(tier>5)
        std::cout << "or you can use superior shards with an accumulating " << (int)pow(2,11-tier)  <<  "% chance of success per shard" << std::endl;
    if(tier>4)
        std::cout << "or you can use brilliant shards with an accumulating " << (int)pow(2,10-tier)  <<  "% chance of success per shard" << std::endl;
    if(tier>3)
        std::cout << "or you can use quality shards with an accumulating " << (int)pow(2,9-tier)  <<  "% chance of success per shard" << std::endl;
    if(tier>2)
        std::cout << "or you can use small shards with an accumulating " << (int)pow(2,8-tier)  <<  "% chance of success per shard" << std::endl;
    if(tier>1)
    {
        std::cout << "or you can use weak shards with an accumulating " << (int)pow(2,7-tier)  <<  "% chance of success per shard" << std::endl;
        std::cout << std::endl;
        std::cout << "if you fail the item will have a small chance of reverting back to +" << (tier-2)  << std::endl;
    }
    std::cout << std::endl;
    std::cout << "0 - to cancel" << std::endl;
    std::cout << "1 - to use weak shards, you have " << p->numberOfItem(WEAKSHARD,7) << std::endl;
    if(tier>1)
        std::cout << "2 - to use small shards, you have " << p->numberOfItem(SMALLSHARD,7) << std::endl;
    if(tier>2)
        std::cout << "3 - to use quality shards, you have " << p->numberOfItem(QUALITYSHARD,7) << std::endl;
    if(tier>3)
        std::cout << "4 - to use brilliant shards, you have " << p->numberOfItem(BRILLIANTSHARD,7) << std::endl;
    if(tier>4)
        std::cout << "5 - to use superior shards, you have " << p->numberOfItem(SUPERIORSHARD,7) << std::endl;
    if(tier>5)
        std::cout << "6 - to use epic shards, you have " << p->numberOfItem(EPICSHARD,7) << std::endl;
    if(tier>6)
        std::cout << "7 - to use legendary shards, you have " << p->numberOfItem(LEGENDARYSHARD,7) << std::endl;
}


/**
 * given an item object it prints its stats
 * @param item
 */
void printItemStats(Item * a)
{
    Printer::printer(a->getColouredName(),"   " , a->getBaseStat());
    if(a->getType()==1)
        std::cout << " attack" << std::endl;
    else
        std::cout << " armour" << std::endl;
    std::cout << " + " << a->getStrength() << " Strength" << std::endl;
    std::cout << " + " << a->getStamina() << " Stamina" << std::endl;
    std::cout << " + " << a->getAgility() << " agility" << std::endl;
    std::cout << std::endl;
}

/**
*prints the shop given the player as a parameter
*/
void printShop(Player * p)
{
    std::cout << Helper::repeatString(9,"\n");
    std::cout << "Enter a number to buy the corresponding item." << std::endl;
    std::cout << "You have "<<p->gold<<" gold to spend" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "Weapons" << std::endl;
    std::cout << "1 - shortsword:           damage 4   20gp	"  << std::endl;
    std::cout << "2 - longsword:            damage 8   100gp" << std::endl;
    std::cout << "3 - waraxe:           damage 12  500gp	" << std::endl;
    std::cout << "4 - claymore:         damage 20  2,000gp	" << std::endl;
    std::cout << "5 - executioners axe:     damage 50  8,000gp	" << std::endl;
    std::cout << "6 - Death's Scythe:       damage 100 25,000gp" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "body" << std::endl;
    std::cout << "7 - leather armour:       armour 5  20gp" << std::endl;
    std::cout << "8 - chainmail :           armour 10 100gp" << std::endl;
    std::cout << "9 - breastplate:          armour 20 500gp" << std::endl;
    std::cout << "10 - scale mail:          armour 50 2,000gp" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "head" << std::endl;
    std::cout << "11 - skullcap:            armour 1  5gp" << std::endl;
    std::cout << "12 - leather hood:        armour 2  20gp" << std::endl;
    std::cout << "13 - steel hat:           armour 4  100gp" << std::endl;
    std::cout << "14 - fullhelm:            armour 10 500gp	" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "shoulders" << std::endl;
    std::cout << "15 - cloth shoulderpads:  armour 1  5gp" << std::endl;
    std::cout << "16 - leather shoulderpads:armour 2  20gp" << std::endl;
    std::cout << "17 - steel pauldrons:     armour 4  100gp	" << std::endl;
    std::cout << "18 - oversized pauldrons: armour 10 500gp	" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "hands" << std::endl;
    std::cout << "19 - silk gloves:         armour 1  5gp	" << std::endl;
    std::cout << "20 - leather gloves:      armour 2  20gp	" << std::endl;
    std::cout << "21 - steel gloves:        armour 4  100gp	" << std::endl;
    std::cout << "22 - spiked gauntlets:    armour 10 500gp	" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "legs" << std::endl;
    std::cout << "23 - snazzy pants:        armour 5  20gp" << std::endl;
    std::cout << "24 - leather chaps :      armour 10 100gp" << std::endl;
    std::cout << "25 - chain leggings:      armour 20 500gp" << std::endl;
    std::cout << "26 - steel platelegs:     armour 50 2,000gp" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "27 - potion		"<<p->scalingCost()<<" gp " << std::endl;
    std::cout << "0 - to leave the shop" << std::endl;
    std::cout << Helper::repeatString(1,"\n");
}

void printArenaComplete(int t)
{
    std::cout << "" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "CONGRATULATIONS!!" << std::endl;
    std::cout << "You have completed the arena in " << t << " days by besting the arena champion." << std::endl;
    std::cout << Helper::repeatString(3,"\n");
}

void printEpicArenaComplete(int t,int difficulty)
{
    std::cout << "" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "	DDDDDDDDDDDDD          		MMMMMMMM               MMMMMMMM" << std::endl;
    std::cout << "	D::::::::::::DDD       		M:::::::M             M:::::::M" << std::endl;
    std::cout << "	D:::::::::::::::DD     		M::::::::M           M::::::::M" << std::endl;
    std::cout << "	DDD:::::DDDDD:::::D    		M:::::::::M         M:::::::::M" << std::endl;
    std::cout << "	  D:::::D    D:::::D   		M::::::::::M       M::::::::::M" << std::endl;
    std::cout << "	  D:::::D     D:::::D  		M:::::::::::M     M:::::::::::M" << std::endl;
    std::cout << "	  D:::::D     D:::::D  		M:::::::M::::M   M::::M:::::::M" << std::endl;
    std::cout << "	  D:::::D     D:::::D  		M::::::M M::::M M::::M M::::::M" << std::endl;
    std::cout << "	  D:::::D     D:::::D  		M::::::M  M::::M::::M  M::::::M" << std::endl;
    std::cout << "	  D:::::D     D:::::D  		M::::::M   M:::::::M   M::::::M" << std::endl;
    std::cout << "	  D:::::D     D:::::D  		M::::::M    M:::::M    M::::::M" << std::endl;
    std::cout << "	  D:::::D    D:::::D   		M::::::M     MMMMM     M::::::M" << std::endl;
    std::cout << "	DDD:::::DDDDD:::::D    		M::::::M               M::::::M" << std::endl;
    std::cout << "	D:::::::::::::::DD     		M::::::M               M::::::M" << std::endl;
    std::cout << "	D::::::::::::DDD      		M::::::M               M::::::M" << std::endl;
    std::cout << "	DDDDDDDDDDDDD         		MMMMMMMM               MMMMMMMM" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "WWWWWWWW                           WWWWWWWW IIIIIIIIII NNNNNNNN        NNNNNNNN" << std::endl;
    std::cout << "W::::::W                           W::::::W I::::::::I N:::::::N       N::::::N" << std::endl;
    std::cout << "W::::::W                           W::::::W I::::::::I N::::::::N      N::::::N" << std::endl;
    std::cout << "W::::::W                           W::::::W II::::::II N:::::::::N     N::::::N" << std::endl;
    std::cout << " W:::::W           WWWWW           W:::::W    I::::I   N::::::::::N    N::::::N" << std::endl;
    std::cout << "  W:::::W         W:::::W         W:::::W     I::::I   N:::::::::::N   N::::::N" << std::endl;
    std::cout << "   W:::::W       W:::::::W       W:::::W      I::::I   N:::::::N::::N  N::::::N" << std::endl;
    std::cout << "    W:::::W     W:::::::::W     W:::::W       I::::I   N::::::N N::::N N::::::N" << std::endl;
    std::cout << "     W:::::W   W:::::W:::::W   W:::::W        I::::I   N::::::N  N::::N:::::::N" << std::endl;
    std::cout << "      W:::::W W:::::W W:::::W W:::::W         I::::I   N::::::N   N:::::::::::N" << std::endl;
    std::cout << "       W:::::W:::::W   W:::::W:::::W          I::::I   N::::::N    N::::::::::N" << std::endl;
    std::cout << "        W:::::::::W     W:::::::::W           I::::I   N::::::N     N:::::::::N" << std::endl;
    std::cout << "         W:::::::W       W:::::::W          II::::::II N::::::N      N::::::::N" << std::endl;
    std::cout << "          W:::::W         W:::::W           I::::::::I N::::::N       N:::::::N" << std::endl;
    std::cout << "           W:::W           W:::W            I::::::::I N::::::N        N::::::N" << std::endl;
    std::cout << "            WWW             WWW             IIIIIIIIII NNNNNNNN         NNNNNNN" << std::endl;
    std::cout << std::endl;
    std::cout << "You have completed the epic arena in " << t << " days by besting God." << std::endl;
    if(difficulty==1)
        std::cout << "There are no challanges left to complete on normal mode however you can start again, keeping all your achievements, perks and talents but resetting everything else." << std::endl;
    else
        std::cout << "The game only gets harder from here or you can start again (keeping achievements and talents)" << std::endl;
        std::cout << "1 - start again" << std::endl;
        std::cout << "2 - continue" << std::endl;
}

void itemCreationMessages(Player * p, int i)
{
    if(i==1)
    {
        std::cout << "What type of item do you wish to make?" << std::endl;
        std::cout << "1 - to make a weapon" << std::endl;
        std::cout << "2 - to make body armour" << std::endl;
        std::cout << "3 - to make headgear" << std::endl;
        std::cout << "4 - to make shoulder armour" << std::endl;
        std::cout << "5 - to make hand armour" << std::endl;
        std::cout << "6 - to make leg armour" << std::endl;
        std::cout << "7 - for info, read this if you havn't done this before because once you have started there is no way to retrieve your materials" << std::endl;
        std::cout << "0 - to exit" << std::endl;
    }
    if(i==2||i==3)
    {
        std::cout << "this is where shards can be used to create items" << std::endl;
        std::cout << "the first shard used determines the items base stat only eg armour or attack" << std::endl;
        std::cout << "Shard required		base stat points	cost per point" << std::endl;
        std::cout << "quality shard 			0-25 			100gp" << std::endl;
        std::cout << "brilliant shard 		0-50 			200gp" << std::endl;
        std::cout << "superior shard 			0-80 			300gp" << std::endl;
        std::cout << "epic shard 			0-120 			400gp" << std::endl;
        std::cout << "legendary shard 		0-200 			1,000gp" << std::endl;
        std::cout << std::endl;
    }
    if(i==2||i==4)
    {
        std::cout << "next you can enchant the item using shards, note that these are the items raw stats and that these can be further increased by enchanting at the blacksmith" << std::endl;
        std::cout << "Shard required		stat points		cost per point" << std::endl;
        std::cout << "quality shard			0-6 		20gp" << std::endl;
        std::cout << "brilliant shard			0-8 		50gp" << std::endl;
        std::cout << "superior shard			0-10 		100gp" << std::endl;
        std::cout << "epic shard			0-12 		5,000gp" << std::endl;
        std::cout << "legendary shard			0-16 		20,000gp" << std::endl;
        std::cout << std::endl;
        std::cout << "Note" << std::endl;
        std::cout << "this process must be done for strength stamina and agility individually ie for a weapon say 'the monkey wrench' with 100 attack 12 strength 12 stamina and  12 agility you would need 4 epic shards and 220,000gp " << std::endl;

    }
    if(i==4||i==3)
    {
        std::cout << std::endl;
        std::cout << "1 - to use a quality shard you have " << p->numberOfItem(QUALITYSHARD,7) << std::endl;
        std::cout << "2 - to use a brilliant shard you have " << p->numberOfItem(BRILLIANTSHARD,7) << std::endl;
        std::cout << "3 - to use a superior shard you have " << p->numberOfItem(SUPERIORSHARD,7) << std::endl;
        std::cout << "4 - to use an Epic shard you have " << p->numberOfItem(EPICSHARD,7) << std::endl;
        std::cout << "5 - to use a Legendary shard you have " << p->numberOfItem(LEGENDARYSHARD,7) << std::endl;
    }

    if(i==2)
    {
        std::cout << "this process gives the same stats for all armour peices so it can be a massive boost in armour for your headgear shoulders and hands" << std::endl;
        std::cout << std::endl;
        std::cout << "WARNING: the process is irreversable and unadjustable once it has begun so make sure you have all your reagents BEFORE you attempt to forge an item." << std::endl;
    }

}

void printMarket(Player *p)
{
    std::cout << "The market is currently selling" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    for(unsigned int i = 0; i<p->marketItems.size(); i++)
    {
        Item *a = p->marketItems.at(i);
        Printer::printer((i+1) , ") " ,  (2*a->getValue()) , "gp	" , a->getColouredName(),"\n" );
        if(a->getType()==1)
            std::cout << "	attack:";
        else if(a->getType()!=7)
            std::cout << "	armour:";
        if(a->getType()!=7)
            std::cout << a->getBaseStat() << "	Str:" << a->getStrength() << "	Sta:" << a->getStamina() << "	Agi:" << a->getAgility() << std::endl;
        std::cout << std::endl;
    }
    std::cout << "what item do you wish to inspect. 0 - to exit" << std::endl;
    std::cout << "you have " << p->gold << "gp" << std::endl;
}

void printCharacters(std::vector<std::string> names)
{
    for(unsigned int i=0;i<names.size();++i){
        Player f = Player(names.at(i));
        printf("                %s \nLevel%3d    Turn %3d    Achievement Points %d\n\n\n",f.name.c_str(),f.level,f.time,f.achievementPoints);
    }
}

void printHelpContents()
{
    std::cout<<"contents"<<std::endl;
    std::cout<<"1 - Ability Scores"<<std::endl;
    std::cout<<"2 - Fighting "<<std::endl;
    std::cout<<"3 - Inventory"<<std::endl;
    std::cout<<"4 - Quests"<<std::endl;
    std::cout<<"5 - Enchanting"<<std::endl;
    std::cout<<"6 - Talents"<<std::endl;
    std::cout<<"7 - Companions"<<std::endl;
    std::cout<<"8 - Hints"<<std::endl;
    std::cout<<"0 - exit\n"<<std::endl;
    std::cout<<""<<std::endl;


}

void printHelpAbilityScores()
{
    std::cout<<"\n\n\n\tAbility Scores\n"<<std::endl;
    std::cout<<"Strength - This purely effects damage and is multiplied with your weapon damage to get your actual damage, therefore a high Strength benifits greatly from a high weapon damage.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Stamina - This increases your health and how much health you gain when you level up. It also increases the strength of potions which are affected ONLY by your stamina.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Agility - This determines who attacks first and how often you get to attack. Agility also increases your critical chance (which decreases every level unless your agility increases).\n"<<std::endl;
    std::cout<<""<<std::endl;
    InputReader::pressEnter();

}

void printHelpFighting()
{
    std::cout<<"\n\n\n\t Fighting \n"<<std::endl;
    std::cout<<"In a Fight you will have 5 options (6 after some investment in the Stamina talent tree)\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Wait - There is no strategic advantage to waiting however it useful in getting some achievements\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Fight - This will cause you to attack your opponent dealing a variable amount of damage based on your strength and weapon damage. Your opponents armour is then subtracted from this value and your actual damage displayed. If you have dealt a Critical Hit the message will have \"!!\" after it and the damage will by higher then normal.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Run away - This will cause you to run away, if your opponent is faster then you they may get in a parting shot which can kill you. Otherwise you will run back to the starting area and you will lose nothing (except your dignity).\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Drink a potion - If you have a potion to drink this will cause you to drink it, healing you an amount dependant upon your stamina, note that this cannot heal you past full health.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Befriend the opponent - After using this ability 10 times on 1 opponenet (not neccessarily in a row) the opponent will become your companion and will aid you in battle. You can control and rename your companion in the companion information section in the menu. Note that some opponents are not befriendable.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"After attacking, successfully drinking a potion, or selecting befriend your turn will end. If your agility is much higher then your opponents then you may have another turn straight away, however if your opponent is much more agile then you they can have multiple attacks in a row.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Winning - If you bring your opponent to 0 health then they have been defeated and you will recieve experience and probably gold. Depending on the difficulty of the creature you have a chance of also receiving an item which can be equipped, disenchanted or sold in the inventory screen. (note that in an arena you will not actually recieve any gold)\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Losing - If your opponents brings you to 0 health you have been defeated and the creature will steal 20% of your gold (rounding up) and you will be sent back to the starting area.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Running away - nothing happens if you run away except you may be killed trying to escape (in which case you have been defeated)\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"If you are fighting multiple opponents you must select which target to attack. If you change your mind after choosing, entering an invalid option for your action will allow you to select a different target.\n"<<std::endl;
    std::cout<<""<<std::endl;
    InputReader::pressEnter();


}

void printHelpInventory()
{
    std::cout<<"\n\n\n\t Inventory \n"<<std::endl;

    std::cout<<"The inventory displays useful information about your character such as your level, health, ability scores (total then the stats from your items in brackets), critical chance, the number of potions you own, honour (currently only used for achievements), total experience, experience until you level up and your gold.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"It also shows you all the items you are wearing and their stats.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"The Inventory Part - this shows all the items in your inventory and their stats. The abrreviations are as follows, a = armour or attack, Str = strength, Sta = stamina, agi = agility and value = the gold you will get if you sell that item.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"After selecting an item (by entering the number before the bracket in front of the item) it will display the item you are currently wearing and the item you have selected. You can then either sell the item (for its value) or equip it.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"If the item has any ability modifiers then you can also disenchant the item to get shards. Note that the power of the shards depends ONLY on the number of ability modifiers the item has ie both a +5 Deaths Scythe (+5 in all stats) and quality leather shoulder pads of the warlord (also +5 in all stats but worth much less) would disenchant for the same thing.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Lastly it shows how many shards you have of each kind.\n"<<std::endl;
    std::cout<<""<<std::endl;
    InputReader::pressEnter();

}

void printHelpQuests()
{
    std::cout<<"\n\n\n\t Quests \n"<<std::endl;
    std::cout<<"Quests are given by the mayer who can be found in the town. You can only have 1 quest at a time and the difficulty scales with your level. You can abandon quests in the quest information section of the menu, you may not aquire another quest that turn.\n"<<std::endl;
    std::cout<<""<<std::endl;
    InputReader::pressEnter();

}

void printHelpEnchanting()
{
    std::cout<<"\n\n\n\t Enchanting \n"<<std::endl;
    std::cout<<"The different Tiers of enchanting are unlocked as Perks from the Achievement Points Achievements.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"To enchant an item requires one or more shards. Shards can drop off creatures and can also be purchased from the market if they are being sold there. However the main way to gain shards is by disenchanting magical items.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Higher tier enchanting requires higher level shards or multiple lower level shards.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Items can be enchanted up to +7"<<std::endl;
    std::cout<<""<<std::endl;
    InputReader::pressEnter();
}

void printHelpTalents()
{
    std::cout<<"\n\n\n\t Talents \n"<<std::endl;
    std::cout<<"Every 10 levels you may pick a talent or put 10 points into a stat of your choice. To qualify for the different talents you require certain ability scores in the chosen tree's speciality. 20 for tier 1, 50 for tier 2, 100 for tier 3 and 200 for tier 4. The three tree specialities are Slayer - Strength, Tank - Stamina and Ninja - Agility.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Talents are kept upon character reset however the 10 stat points are not.\n"<<std::endl;
    std::cout<<""<<std::endl;
    InputReader::pressEnter();
}

void printHelpCompanions()
{
    std::cout<<"\n\n\n\t Companions \n"<<std::endl;
    std::cout<<"You can befriend most creatures by making them become your companion.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"It takes 10 turns of befriending a creature to make it your companion.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"You can rename and set your companion to passive or aggressive in the companion info page (found in the main menu).\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"An aggressive companion will aid you in combat, attacking your foes. A passive companion will sit idly by.\n"<<std::endl;
    std::cout<<""<<std::endl;
    InputReader::pressEnter();

}

void printHelpHints()
{
    std::cout<<"\n\n\n\t Hints \n"<<std::endl;
    std::cout<<"Achievements are the key to success, many of them grant very useful perks so always look at your perks regularly. This can be accessed in the menu-achievements section. You can also see the requirements for different achievements in the unearned achievements section.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"The zones are arranged in order of difficulty eg. park then forest then mountains. The arena contains all the creatures in order of difficulty but you won't regain health between each battle.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"It takes 1 turn for every trip into the single fight zones or one entry into the arena.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"The Reward is better the higher round you retire at in the arena, however you get nothing if you die or run away so be careful as to how far you go (potions are very good for emergencies).\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Enchanting an item raises all its stats so it is often worth it to enchant an item before disenchanting it, as you will often get much better shards (if the item was initially good).\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"The cost of potions scale with your level and jumps every 10 levels so its a good idea to buy them early.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Shards are usually fairly cheap in the market so visit it often and snap up any weak shards you see. Cheap magical items can also be bought and disenchanted.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Your damage is multiplied by weapon damage so get a weapon early to replace your stick.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Items are always cheaper in the shop then the market so if possible buy your items in the shop.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"You start off with money and potions so use them wisely especially if you're going for achievements.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"After you defeat the arena, Item creation is unlocked in the blacksmith in town. Using this you can create far more powerful items then you could come across in the game normally.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"The bestiary shows the stats of the creatures you fought/defeated these can be useful in determining future tactics. Eg what companion is best for you.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"Companions can vary in usefulness depending on your characters build. They are far more useful to a stamina build then to a strength/agility build.\n"<<std::endl;
    InputReader::pressEnter();
    std::cout<<"PS. max level is 100, also for those who bothered to read this, the secret area is accessable at 1337.\n"<<std::endl;
    std::cout<<""<<std::endl;
    InputReader::pressEnter();

}

}
namespace printerHelper{
void print(std::stringstream &ss){

    ScrUtil::Attributes original = ScrUtil::getCurrentAttributes();
    char current = ss.get();
    bool coloured = false;
    while(ss.good())
    {
        if(current=='`')
        {
            if(coloured)
            {
                ScrUtil::setColors(original);
                coloured = false;
            }
            else
            {
                char colour = ss.get();
                switch(colour){
                    case 'r':ScrUtil::setColors(ScrUtil::Red,original.paper);break;
                    case 'b':ScrUtil::setColors(ScrUtil::Blue,original.paper);break;
                    case 'g':ScrUtil::setColors(ScrUtil::Green,original.paper);break;
                    case 'c':ScrUtil::setColors(ScrUtil::Cyan,original.paper);break;
                    case 'y':ScrUtil::setColors(ScrUtil::Yellow,original.paper);break;
                    case 'm':ScrUtil::setColors(ScrUtil::Magenta,original.paper);break;
                    case 'w':ScrUtil::setColors(ScrUtil::White,original.paper);break;
                }
                coloured = true;
            }
        }
        else
        {
            std::cout<<current;
        }
        current = ss.get();
    }
}
}

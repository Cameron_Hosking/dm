#ifndef CREEP_H
#define CREEP_H
#include "Helper.h"
class Creep
{
public:
    Creep(){}
    Creep(std::string name)
        :creep(name), kills(0),deaths(0),flees(0),encountered(false)
    {}
    void initilize(int clife, int cdamage, int cagility, std::string cattack, std::string carmour, int cdr, int cxp, int cgp, double clootChance, int clootValue);
    void result(int i);
    void setEncounterStats(bool encountered, int kills, int deaths, int flees);
    void hasBeenEncountered(){encountered = true;}
    void printStats();
    bool equals(Creep *c);
    bool equals(std::string name);
    std::string getName(){return creep;}
    int getLife(){return life;}
    int getDamage(){return damage;}
    int getAgility(){return agility;}
    std::string getAttack(){return attack;}
    std::string getArmour(){return armour;}
    int getDr(){return dr;}
    int getXp(){return xp;}
    int getGp(){return gp;}
    bool isEncountered(){return encountered;}
    int getKills(){return kills;}
    int getDeaths(){return deaths;}
    int getFlees(){return flees;}
    double getLootChance(){return lootChance;}
    int getLootValue(){return lootValue;}


private:
    std::string creep;
        int life;
        int damage;
        int agility;
        std::string attack;
        std::string armour;
        int dr;
        int xp;
        int gp;
        int kills;
        int deaths;
        int flees;
        bool encountered;
        double lootChance;
        int lootValue;
};

#endif // CREEP_H

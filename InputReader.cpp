#include "InputReader.h"
#include <limits>
namespace InputReader{
int getInt(){
    int input;
    std::cin >> input;
    while(std::cin.fail())
    {
        std::cout << "please input an integer" << std::endl;
        std::cin.clear();
        std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
        std::cin >> input;
    }
    return input;
}

double getDouble(){
    double input;
    std::cin >> input;
    while(std::cin.fail())
    {
        std::cout << "please input an double" << std::endl;
        std::cin.clear();
        std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
        std::cin >> input;
    }
    return input;
}

std::string getString(){
    char input[256];
    std::string bin;
    std::cin >> bin;
    std::cin.getline (input,256);
    bin.append(input);
    autoPressEnter();

    return bin;
}

void pressEnter(){
std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
std::cin.get ();
autoPressEnter();
}
void autoPressEnter(){
    std::cin.putback('\n');
}
}
